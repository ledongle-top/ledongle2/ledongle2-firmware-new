#include "effect_color_setter.h"
#include <esp_log.h>

void set_effect_color(EffectArguments *args, uint32_t color){
  switch(args -> effect_arguments_case){
#include "effect_color_setter.cblock"
  default:
    ESP_LOGE("COLOR_SETTER", "Failed to set effect color");
    break;
  }
}
