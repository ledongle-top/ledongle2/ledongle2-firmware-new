#define WS2812_LIB
#include <WS2812_effects.h>
#include <WS2812_strip.h>
#include <stdlib.h>
#include <esp_log.h>
#define VERYSHORTDELAY 10
#define SHORTDELAY 20
#define LONGDELAY 50

static const char* TAG = "WS2812EFFECTS";
WS2812_strip::WS2812_strip():brightness(0),effect_state(NULL) {}

struct RGB Wheel(uint8_t WheelPos);

void adjustBrightnessRGB(struct RGB *rgb, uint8_t brightness){
  /*if(brightness == 255) return;
  uint16_t multiplied = (uint16_t)rgb->r * (uint16_t) brightness;
  rgb ->r = multiplied >> 8;
  multiplied = (uint16_t)rgb->g * (uint16_t) brightness;
  rgb ->g = multiplied >> 8;
  multiplied = (uint16_t)rgb->b * (uint16_t) brightness;
  rgb ->b = multiplied >> 8;*/
}
int mod(int x, int m){
  while(x < 0){
    x += m;
  }
  while(x > m){
    x-=m;
  }
  return x;
}


/*
int WS2812_strip::strobe(){
  if(effect_state == NULL)  effect_state = calloc(sizeof(uint8_t), 1);
  uint8_t *state = (uint8_t*) effect_state;
  if( *state == 0){
    setAllLeds(r,g,b);
    *state = 1;
  }else{
   setAllLeds(0,0,0);
   *state = 0;
  }
  return 3*LONGDELAY;
}


int WS2812_strip::christmas(){
  if(effect_state == NULL)  effect_state = calloc(sizeof(uint8_t), 1);
  uint8_t *state = (uint8_t*) effect_state;

  if(*state == 0){
    for(uint16_t i=0;i<num; i+=2){
      setLed(i,brightness,0,0);
    }
    for(uint16_t i=1;i<num; i+=2){
      setLed(i,0,brightness,0);
    }

  }else{
    for(uint16_t i=0;i<num; i+=2){
      setLed(i,0,brightness,0);
    }
    for(uint16_t i=1;i<num; i+=2){
      setLed(i,brightness,0,0);
    }
  }
  *state = !*state;
  return 500;
}

struct pulse_state{
  uint8_t index;
  uint16_t cur_r;
  uint16_t cur_g;
  uint16_t cur_b;
};
int WS2812_strip::pulse(){
  if(effect_state == NULL){
     effect_state = calloc(sizeof(struct pulse_state), 1);
  }
  struct pulse_state *state = (struct pulse_state *)  effect_state;
  uint16_t inc_r = r << 2;
  uint16_t inc_g = g << 2;
  uint16_t inc_b = b << 2;

  if(state->index < 64){
    state->cur_r+=inc_r;
    state->cur_g+=inc_g;
    state->cur_b+=inc_b;
    for(uint16_t i=0; i<num;i++){
      setLed(i,
             *(((uint8_t*)&state->cur_r)+1),
             *(((uint8_t*)&state->cur_g)+1),
             *(((uint8_t*)&state->cur_b)+1));
    }
  }else{
    state->cur_r-=inc_r;
    state->cur_g-=inc_g;
    state->cur_b-=inc_b;
    for(uint16_t i=0; i<num;i++){
      setLed(i,
             *(((uint8_t*)&state->cur_r)+1),
             *(((uint8_t*)&state->cur_g)+1),
             *(((uint8_t*)&state->cur_b)+1));
    }

  }
  state -> index = (state->index == 127)? 0 : (state -> index +1);

  return SHORTDELAY;
}

struct rainbowPulse_state{
  struct pulse_state pulseState;
  uint8_t round;
};
int WS2812_strip::rainbowPulse(){
  if( effect_state == NULL){
     effect_state = calloc(sizeof(struct rainbowPulse_state), 1);
  }
  struct rainbowPulse_state *state = (struct rainbowPulse_state*)  effect_state;

  if(state -> pulseState.index == 0){
    state -> round = (state->round == 15)? 0: (state->round+1);
    struct RGB rgb=Wheel((state->round)*16);
    r = rgb.r;
    g = rgb.g;
    b = rgb.b;
  }
  int res =  pulse();
  return res;
}

int WS2812_strip::randomPulse(){
  struct pulse_state *state = (struct pulse_state *)  effect_state;
  if((state == NULL) || (state -> index == 0)){
    struct RGB rgb=Wheel(rand());
    r = rgb.r;
    g = rgb.g;
    b = rgb.b;
  }
  return pulse();
}
int fire(){
  for(uint16_t i=0;i<numLeds;){
    uint32_t randomNum = random();
    for(uint8_t j=0;j<10;j++,i++){
      uint8_t shift = randomNum & 0x3;
      setLed(leds,i,r>>shift,g>>shift,b>>shift);
      randomNum = randomNum >> 3;
    }
  }
  //for(uint8_t i=0;i<10;i++){
    LONGDELAY;
    //}
 abort:
    return;
    }
int WS2812_strip::wipe(){

  if(effect_state == NULL)  effect_state = calloc(sizeof(uint16_t), 1);
  uint16_t *i = (uint16_t*) effect_state;
  ESP_LOGD(TAG, "wipe pixel %u", *i);
  if(*i<num){
      setLed(*i,r,g,b);
      (*i)++;
      return LONGDELAY;
    }else{
      return -1;
    }

}

int WS2812_strip::nuigurumi(){
  for(uint8_t i=0; i<30;i++){
    setLed( i, 255, 255,0);
  }for(uint8_t i=30; i<60;i++){
    setLed( i, 0, 255,255);
  }for(uint8_t i=60; i<90;i++){
    setLed( i, 255, 0,255);
  }
  return -1;
}

int WS2812_strip::rainbow(){
  if(effect_state == NULL)  effect_state = calloc(sizeof(uint8_t), 1);
  uint8_t *j = (uint8_t*) effect_state;
  for(uint16_t i=0; i<num; i++) {
      struct RGB next = Wheel(*j);
      next.r = adjustBrightness(next.r,  brightness);
      next.g = adjustBrightness(next.g,  brightness);
      next.b = adjustBrightness(next.b,  brightness);
      setLed(i, next.r,next.g,next.b);
    }
    (*j)++;
    return SHORTDELAY;
}

int WS2812_strip::rainbowCycle(){
  if(effect_state == NULL)  effect_state = calloc(sizeof(uint16_t), 1);
  uint16_t *j = (uint16_t*) effect_state;

  for(uint16_t i=0; i< num; i++) {
    struct RGB next = Wheel((uint8_t)(((i * 256 / num) + *j)));
    next.r = adjustBrightness(next.r,  brightness);
    next.g = adjustBrightness(next.g,  brightness);
    next.b = adjustBrightness(next.b,  brightness);
    setLed(i, next.r,next.g,next.b);
  }
  (*j) = ((*j) == (256*5)) ? 0: (*j)+1;
  return SHORTDELAY;
}

int WS2812_strip::theaterChase(){
  if(effect_state == NULL)  effect_state = calloc(sizeof(uint8_t), 1);
  uint8_t *q = (uint8_t*) effect_state;
  for (uint16_t i=0; i+*q < ( num); i=i+3){
    setLed(i+*q, 0, 0, 0);        //turn every third pixel off
  }
  *q = (*q) == 2 ? 0: (*q)+1;
  for(uint16_t i=0; i+*q < ( num); i=i+3) {
    setLed(i+(*q), r,g,b);    //turn every third pixel on
  }
  return 2*LONGDELAY;
}

int WS2812_strip::theaterChaseRainbow(){
  if(effect_state == NULL) effect_state = calloc(sizeof(uint8_t), 2);
  uint8_t *q = (uint8_t*)effect_state;
  uint8_t *j = ((uint8_t*)effect_state) +1;
  for (uint16_t i=0; (i+*q) < num; i=i+3){
    setLed(i+*q, 0, 0, 0);        //turn every third pixel off
  }
  *q = (*q) == 2 ? 0: (*q)+1;
  for(uint16_t i=0; i+*q < num; i=i+3) {
    struct RGB next = Wheel(i+(*j));
    next.r = adjustBrightness(next.r, brightness);
    next.g = adjustBrightness(next.g, brightness);
    next.b = adjustBrightness(next.b, brightness);
    setLed(i+*q, next.r,next.g,next.b);    //turn every third pixel on
  }
  *j = (*j)+1;
  return LONGDELAY;
}


int WS2812_strip::staticRainbow(){
  uint8_t colorStep = 255 /  num;
  if(colorStep == 0) colorStep = 1;
  uint8_t brightness = MAX(MAX(r, g), b);
  for(uint16_t i =0; i< num; i++){
    struct RGB color = Wheel(i*colorStep);
    adjustBrightnessRGB(&color, brightness);
    setLed( i, color.r, color.g, color.b);
  }
  return -1;
}

int WS2812_strip::rainbowWave(){
  uint8_t fullBrightness = MAX(MAX(r, g), b);
  uint8_t quarterBrightness = fullBrightness/4;
   r =  g =  b = quarterBrightness;
  staticRainbow();
   r =  g =  b = fullBrightness;

  if(effect_state == NULL)  effect_state = calloc(sizeof(uint16_t), 1);
  uint16_t *i = (uint16_t*) effect_state;

  uint8_t colorStep = 255 /  num;
  if(colorStep == 0) colorStep = 1;
  for(uint16_t j = 0; j < 3; j++){
    struct RGB color = Wheel(((*i+j)%num) * colorStep);
    adjustBrightnessRGB(&color, fullBrightness - j*quarterBrightness);
    setLed( ((*i + j)%  num),  color.r, color.g, color.b);
  }
  for(uint16_t j = 1; j < 3; j++){
    struct RGB color = Wheel(((*i-j)% num) * colorStep);
    adjustBrightnessRGB(&color, fullBrightness - j*quarterBrightness);
    setLed( ((*i - j)%  num),  color.r, color.g, color.b);
  }
  *i = (*i ==( num - 1))? 0: (*i+1);
  return 100;
}

int WS2812_strip::wave(){
  if(effect_state == NULL)  effect_state = calloc(sizeof(uint16_t), 1);
  uint16_t *j = (uint16_t*) effect_state;
    for(uint16_t i=0;i < num;i++){
      setLed(i,(r)/8,(g)/8,(b)/8);
    }
    for(uint8_t k=0;k<5;k++){
      setLed(((*j)+k)% num,r,g,b);
    }
    *j = (*j) == (num) -1?0:(*j)+1;
    return 100;
}
struct sparklerState{
  uint16_t position;
  uint8_t timeAtPosition;
};
int WS2812_strip::sparkler(){
  if( effect_state == NULL){
    effect_state = calloc(sizeof(struct sparklerState), 1);
    *(uint16_t*)effect_state =  num - 2;
  }
  struct sparklerState *state = (struct sparklerState *)  effect_state;
  //Advance one LED every 2 sec
  if(state->timeAtPosition == 20){
    state->timeAtPosition = 0;
    state -> position = (state->position == 1)? ( num -2): (state->position -1);
    resetAllLeds();
  }
  uint8_t sparkle = rand() % 100;
  setLed(state->position, 150 + sparkle, 130+sparkle, 50);
  setLed(state->position-1, 50 + sparkle, 30+sparkle, 20);
  setLed(state->position+1, 50 + sparkle, 30+sparkle, 20);
  if(state->position -2 >= 0){
    if( sparkle > 50){
    setLed(state->position-2, 10+ sparkle, sparkle, 20);
  }else{
    setLed(state->position-2, 0, 0, 0);
  }
  }
  if(state->position +2 < num ){
    if( sparkle > 50){
      setLed(state->position+2, 10 + sparkle, sparkle, 20);
    }else{
      setLed(state->position+2, 0, 0, 0);
    }
  }
  state -> timeAtPosition++;
  return 100;
}


int WS2812_strip::snake(){
  uint8_t foodNum = ( num/10) +1;
  int16_t *snakePos = (int16_t*)effect_state;
  int16_t *snakeSize = ((int16_t*)effect_state)+1;
  uint16_t *snakeFood= ((uint16_t*)effect_state)+2;

  if(effect_state == NULL){
     effect_state = calloc(sizeof(uint16_t), foodNum + 2);
    snakePos = (int16_t*)effect_state;
    snakeSize = ((int16_t*)effect_state)+1;
    snakeFood= ((uint16_t*)effect_state)+2;

    *snakePos = 1;
    *snakeSize = 1;
    for(uint8_t i=0; i<foodNum;i++){
      snakeFood[i]=rand()% num;
    }
  }


  if(*snakeSize >=  num){
    *snakeSize=0;
    resetAllLeds();
  }

  for(uint8_t i=0;i<foodNum;i++){
    if(snakeFood[i]==*snakePos){
      *snakeSize = *snakeSize + 1;
      snakeFood[i]=rand()% num;
    }
    setLed(snakeFood[i],r,g,b);
  }
  setLed(*snakePos,r,g,b);
  for(uint16_t j=*snakeSize;j>0;j--){
    setLed(mod((*snakePos-j), num),r,g,b);
  }
  setLed(mod((*snakePos-*snakeSize), num),0,0,0);
  *snakePos = (*snakePos == (num-1))? 0 : (*snakePos +1);
  return 200;
}

struct RGB Wheel(uint8_t WheelPos) {

}

int WS2812_strip::sparkle(){
  uint8_t numSparks =  num/15 +1;
  setAllLeds(r/8,g/8,b/8);
  for(uint8_t i = 0; i < numSparks; i++){
      uint8_t spark =(rand()% num);
      setLed(spark,r,g,b);
  }
  return 500;
}
void WS2812_strip::resetState(){
  if(effect_state != NULL){
    free(effect_state);
    effect_state = NULL;
  }
  resetAllLeds();
}




void WS2812_strip::setColor(int r, int g, int b, int brightness){
  if(r > 255 || g > 255 || b > 255 || r < -1 || g < -1 || b < -1){
    ESP_LOGE(TAG, "Invalid color settings: %d %d %d", r , g, b);
    return;
  }
  this->r = r == -1 ? this->r:r;
  this->g = g == -1 ? this->g:g;
  this->b = b == -1 ? this->b:b;
  ESP_LOGI(TAG, "change color r: %d g:%d b:%d bright: %d", r,g,b,brightness);

  this->brightness = brightness == -1 ? MAX(this->r, MAX(this->g, this->b)):
    brightness;
  resetState();
}


void WS2812_strip::setEffect(enum LED_PATTERN cmd){
  ESP_LOGI(TAG, "change effect cmd: %d", cmd);
  this->cmd = cmd;
  resetState();
}

int WS2812_strip::colorWipe(){
  if(effect_state == NULL)  effect_state = calloc(sizeof(uint16_t), 1);
  uint16_t *i = (uint16_t*) effect_state;
  if(*i == 0){
    struct RGB color = Wheel(rand());
    adjustBrightnessRGB(&color,  brightness);
     r = color.r;
     g = color.g;
     b = color.b;
  }
  setLed(*i,r,g,b);
  (*i)++;
  if(*i == num){
    *i = 0;
  }
  return 2 * LONGDELAY;
}

int WS2812_strip::wipeRandom(){
  if(effect_state == NULL)  effect_state = calloc(sizeof(uint16_t), 1);
  uint16_t *i = (uint16_t*) effect_state;
  if(*i == 0){
    struct RGB color = Wheel(rand());
    adjustBrightnessRGB(&color,  brightness);
     r = color.r;
     g = color.g;
     b = color.b;
  }
  if(*i < num){
    setLed(*i,r,g,b);
    (*i)++;
  }else{
    setLed(num - (*i - num) -1,0,0,0);
    (*i)++;
    if(*i == (2*num) -1){
      *i = 0;
    }
  }
  if(*i ==  num){
    return 500;
  }else{
    return 2*LONGDELAY;
  }
}

int WS2812_strip::wipeOnOff(){
  if(effect_state == NULL)  effect_state = calloc(sizeof(uint16_t), 1);
  uint16_t *i = (uint16_t*) effect_state;
  if(*i < num){
    setLed(*i,r,g,b);
    (*i)++;
  }else{
    setLed(num - (*i - num) -1,0,0,0);
    (*i)++;
    if(*i == (2*num) -1){
      *i = 0;
    }
  }
  if(*i ==  num){
    return 500;
  }else{
    return 2*LONGDELAY;
  }

}

struct blob_state{
  uint16_t position;
  uint8_t frame;
  struct pulse_state inner_state;
  struct pulse_state outer_state;
};
int WS2812_strip::colorBlobs(){
  if(effect_state == NULL)  effect_state = calloc(sizeof(struct blob_state),1);
  struct blob_state *state = (struct blob_state *) effect_state;
  if((state -> frame) == 0){//make new blob
    struct RGB color = Wheel(rand());
    adjustBrightnessRGB(&color,  brightness);
     r = color.r;
     g = color.g;
     b = color.b;
    state -> position = rand() / (RAND_MAX / num + 1);
    //state -> position = random()%num;
    state -> frame = 1;
    return 0;
  }else{//expand current blob;
    uint16_t inc_r = r << 4;
    uint16_t inc_g = g << 4;
    uint16_t inc_b = b << 4;

    state -> inner_state.cur_r += inc_r;
    state -> inner_state.cur_g += inc_g;
    state -> inner_state.cur_b += inc_b;

    if(state -> frame > 8){
      state -> outer_state.cur_r += inc_r;
      state -> outer_state.cur_g += inc_g;
      state -> outer_state.cur_b += inc_b;
    }

    if(state -> frame <= 8){
      setLed(state->position,
                  *(((uint8_t*)&state->inner_state.cur_r)+1),
                  *(((uint8_t*)&state->inner_state.cur_g)+1),
                  *(((uint8_t*)&state->inner_state.cur_b)+1));
    }else if(state -> frame <= 16){
      setLed(state->position,
                  *(((uint8_t*)&state->inner_state.cur_r)+1),
                  *(((uint8_t*)&state->inner_state.cur_g)+1),
                  *(((uint8_t*)&state->inner_state.cur_b)+1));
      setLed(state->position+1,
                  *(((uint8_t*)&state->outer_state.cur_r)+1),
                  *(((uint8_t*)&state->outer_state.cur_g)+1),
                  *(((uint8_t*)&state->outer_state.cur_b)+1));
      setLed(state->position-1,
                  *(((uint8_t*)&state->outer_state.cur_r)+1),
                  *(((uint8_t*)&state->outer_state.cur_g)+1),
                  *(((uint8_t*)&state->outer_state.cur_b)+1));
      state -> frame++;
      if(state -> frame > 16){
        memcpy(&state -> inner_state, &state -> outer_state ,sizeof(struct pulse_state));
        memset(&state -> outer_state, 0, sizeof(struct pulse_state));
      }
    }else if(state -> frame <= 24){
      setLed(state->position+1,
                  *(((uint8_t*)&state->inner_state.cur_r)+1),
                  *(((uint8_t*)&state->inner_state.cur_g)+1),
                  *(((uint8_t*)&state->inner_state.cur_b)+1));
      setLed(state->position-1,
                  *(((uint8_t*)&state->inner_state.cur_r)+1),
                  *(((uint8_t*)&state->inner_state.cur_g)+1),
                  *(((uint8_t*)&state->inner_state.cur_b)+1));
      setLed(state->position+2,
                  *(((uint8_t*)&state->outer_state.cur_r)+1),
                  *(((uint8_t*)&state->outer_state.cur_g)+1),
                  *(((uint8_t*)&state->outer_state.cur_b)+1));
      setLed(state->position-2,
                  *(((uint8_t*)&state->outer_state.cur_r)+1),
                  *(((uint8_t*)&state->outer_state.cur_g)+1),
                  *(((uint8_t*)&state->outer_state.cur_b)+1));
      if(state -> frame > 24){
        memcpy(&state -> inner_state, &state -> outer_state ,sizeof(struct pulse_state));
        memset(&state -> outer_state, 0, sizeof(struct pulse_state));
      }
    }else if(state -> frame <= 32){
      setLed(state->position+2,
                  *(((uint8_t*)&state->inner_state.cur_r)+1),
                  *(((uint8_t*)&state->inner_state.cur_g)+1),
                  *(((uint8_t*)&state->inner_state.cur_b)+1));
      setLed(state->position-2,
                  *(((uint8_t*)&state->inner_state.cur_r)+1),
                  *(((uint8_t*)&state->inner_state.cur_g)+1),
                  *(((uint8_t*)&state->inner_state.cur_b)+1));
    }else{
      memset(effect_state, 0, sizeof(struct blob_state));
      return 1000;
    }
    state -> frame ++;
    return VERYSHORTDELAY;

  }

}



int WS2812_strip::executeEffect(){
  int res;
  if(num == 0){
    ESP_LOGW(TAG, "0 LEDs, not executing");
    return -1;
  }
  switch (cmd){
  case SOLID:
    res = wipe();
    break;
  case RAINBOW:
    res = rainbow();
    break;
  case RAINBOW_CYCLE:
    res = rainbowCycle();
    break;
  case RAINBOW_CHASE:
    res = theaterChaseRainbow();
    break;
  case PULSE:
    res = pulse();
    break;
  case CHASE:
    res = theaterChase();
    break;
  case CHRISTMAS:
    res = christmas();
    break;
  case SNAKE:
    res = snake();
    break;
  case WAVE:
    res = wave();
    break;
  case SPARK:
    res = sparkle();
    break;
  case RAINBOW_PULSE:
    res = rainbowPulse();
    break;
  case RANDOM_PULSE:
    res = randomPulse();
    break;
  case STROBE:
    res = strobe();
    break;
  case SPARKLER:
    res = sparkler();
    break;
  case STATIC_RAINBOW:
    res = staticRainbow();
    break;
  case RAINBOW_WAVE:
    res = rainbowWave();
    break;
  case COLOR_WIPE:
    res = colorWipe();
    break;
  case WIPE_ON_OFF:
    res = wipeOnOff();
    break;
  case ON_OFF_RANDOM:
    res = wipeRandom();
    break;
  case COLOR_BLOBS:
    res = colorBlobs();
    break;
  case NUIGURUMI:
    res = nuigurumi();
    break;
  case COMMAND_NONE:
    res=-1;
    break;
  default:
    ESP_LOGW(TAG, "unknown CMD: %d", cmd);
    res = -1;
    break;
  }
  showFrame();
  return res;
}




int WS2812_strip::blink(){
  if(effect_state == NULL)  effect_state = calloc(sizeof(uint8_t), 1);
  uint8_t *i = (uint8_t*) effect_state;
  if(*i == 0){
    setAllLeds(r, g, b);
    *i = 1;
    return 500;
  }else{
    resetAllLeds();
    return -1;
    return 0;
  }
}
*/
