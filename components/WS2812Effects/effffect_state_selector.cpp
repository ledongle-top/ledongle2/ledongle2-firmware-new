#include "effect_selector.h"
#include "strip_config.pb-c.h"
#include <stdlib.h>
#include <esp_log.h>
#include <esp_err.h>


void generate_effect_state(EffectArguments* effect_arguments, EffectState *effect_state){
  void *res = NULL;
  switch(effect_arguments -> effect_arguments_case){
    #include "effect_state_selector.cblock"
  default:
    ESP_LOGE("EFFECT_SELECTOR", "FAILED TO GENERATE EFFECT STATE");
    ESP_ERROR_CHECK(ESP_FAIL);
  }
}
