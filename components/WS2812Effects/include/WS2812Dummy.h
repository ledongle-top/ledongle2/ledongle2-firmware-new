#ifndef WS2812DUMMY_H
#define WS2812DUMMY_H
#include "WS2812_strip.h"
#include "string.h"
class WS2812Dummy: public WS2812_strip{
 private:
  void setLed(uint16_t i, uint8_t r, uint8_t g, uint8_t b);
  void setAllLeds(uint8_t r, uint8_t g, uint8_t b);
  void showFrame();
  void setNum(int num);
  void setBrightness(uint8_t brightness);
public:
  uint16_t num;
  uint8_t *buffer;
  void resetAllLeds();
  WS2812Dummy(size_t num);
  ~WS2812Dummy();
};
#endif
