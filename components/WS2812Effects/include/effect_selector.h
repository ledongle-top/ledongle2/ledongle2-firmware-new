#include "WS2812_strip.h"
#include "effect_arguments.pb-c.h"
void (*select_effect(EffectArguments__EffectArgumentsCase argType))(WS2812_strip*, void*, void*);

void generate_effect_state(EffectArguments *args, EffectState *state);
