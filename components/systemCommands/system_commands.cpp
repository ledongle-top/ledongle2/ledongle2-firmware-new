#include "system_commands.h"
#include "ota.h"
#include "system_config.h"
#include "ledongle_wifi.h"
static const char* TAG = "SYSCMD";
void execute_system_command(SystemCommand *command){
  if(command == NULL){
    ESP_LOGW(TAG, "Command was null");
    return;
  }
  switch(command -> type){
  case SYSTEM_COMMAND_TYPE__CHECK_OTA_UPDATE:{
    ota_check_for_update();
    break;
  }
  case SYSTEM_COMMAND_TYPE__START_OTA_UPDATE:{
    ota_start_update();
    break;
  }
  case   SYSTEM_COMMAND_TYPE__WIFI_CONNECT:{
    wifi_connect();
    break;
  }
  case SYSTEM_COMMAND_TYPE__LIGHTS_OFF:{
    ESP_LOGI(TAG, "Lights off");
    for(int i = 0; i < systemConfig -> n_strip_config; i++){
      toggle_strip(i, false);
    }
    break;
  }
  case SYSTEM_COMMAND_TYPE__LIGHTS_ON:{
    ESP_LOGI(TAG, "Lights on");
    for(int i = 0; i < systemConfig -> n_strip_config; i++){
      toggle_strip(i, true);
    }
    break;
  }
  default:
    ESP_LOGW(TAG, "Command was not recognized");
    break;
  }
}
