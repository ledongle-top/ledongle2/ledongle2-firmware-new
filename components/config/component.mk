COMPONENT_SRCDIRS := . effects
COMPONENT_ADD_INCLUDEDIRS := include effects
PROTOS = $(wildcard **/*.proto)

$(COMPONENT_LIBRARY): $(PROTOS)

%.proto:
	cp $@ $(BUILD_DIR_BASE)/protos/
