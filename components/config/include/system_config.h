#ifndef SYSTEM_CONFIG_H
#define SYSTEM_CONFIG_H

#include <system_config.pb-c.h>
#include <system_status.pb-c.h>
#include <effect_arguments.pb-c.h>
#include <esp_err.h>
#include <WS2812Runner.h>
#include <stdbool.h>
#include "freertos/event_groups.h"

#define SYSTEM_CONFIG_UPDATE (1 << 0)
#define SYSTEM_STATUS_UPDATE (1 << 1)
#define EFFECT_ARGUMENTS_UPDATE (1 << 2)

extern SystemConfig *systemConfig;
extern SystemStatus *systemStatus;
extern WS2812Runner **strip_runners;

esp_err_t initialize_system_config();
esp_err_t set_system_config(SystemConfig *newConfig);
void set_strip_args(uint strip_id, EffectArguments *args, bool from_sync);
esp_err_t persist_config(SystemConfig *config);
void strip_next_color(uint strip_idx, bool backwards);
void toggle_strip(int i, bool state);
void update_system_status(SystemStatus *new_status);
EventBits_t wait_for_status_update(int update_type);
esp_err_t reset_config(void);

#endif
