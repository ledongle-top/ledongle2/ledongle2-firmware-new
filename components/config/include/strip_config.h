syntax = "proto3";

message StripConfig{
  uint32 strip_pin = 1;
  uint32 leds = 2;
  uint32 max_brightness = 3;
  oneof effect_arguments{
    SolidArguments solid = 100;
  }
  oneof effect_state{
    SolidState solid = 200;
  }
}
