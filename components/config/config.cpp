#include "system_config.h"
#include "nvs_flash.h"
#include "nvs.h"
#include <esp_log.h>
#include <string.h>
#include <constants.h>
#include <esp_system.h>
#include "WS2812_effects.h"
#include "WS2812_dled.h"
#include "color.h"
#include <protos.h>
#include <blinksync.h>
#include <input.h>
#include <generate_uuid.h>
#include <effect_color_setter.h>
#include <esp_ota_ops.h>
#include "freertos/task.h"
#include <esp_heap_caps.h>
#include <ota.h>
#include <rom/md5_hash.h>

static const char *CONFIGURATION_STORAGE_NAMESPACE = "CONFIG";
static const char *CONFIG_NVS_KEY = "SYSTEM_CONFIG";

static const char* TAG = "CONFIG";
SystemConfig *systemConfig;
SystemStatus *systemStatus;

WS2812Runner **strip_runners;

EventGroupHandle_t status_update_events;

void md5hash(uint8_t* const data, size_t data_len, uint8_t *hash){
  struct MD5Context myContext;
  MD5Init(&myContext);
  MD5Update(&myContext, data, data_len);
  MD5Final(hash, &myContext);
}

esp_err_t persist_config(SystemConfig *config){
  size_t len = system_config__get_packed_size(config);
  esp_err_t err = 0;
  void *buf = malloc(len);
  system_config__pack(config,(uint8_t *)buf);
  nvs_handle handle;
  nvs_open(CONFIGURATION_STORAGE_NAMESPACE, NVS_READWRITE, &handle);
  err = nvs_set_blob(handle, CONFIG_NVS_KEY, buf, len);
  nvs_commit(handle);
  nvs_close(handle);
  if(err){
    ESP_LOGE(TAG, "Failed to persist config");
  }else{
    ESP_LOGI(TAG, "Persisted config to NVS");
  }
  return err;
}

static esp_err_t persist_strip_args(uint strip_id, EffectArguments *args){
  size_t len = effect_arguments__get_packed_size(args);
  esp_err_t err = 0;
  void *buf = malloc(len);
  effect_arguments__pack(args,(uint8_t *)buf);
  char *key = (char*)malloc(8);
  snprintf(key, 8, "args%u", strip_id);
  nvs_handle handle;
  nvs_open(CONFIGURATION_STORAGE_NAMESPACE, NVS_READWRITE, &handle);
  err = nvs_set_blob(handle, key, buf, len);
  nvs_commit(handle);
  nvs_close(handle);
  if(err){
    ESP_LOGW(TAG, "Failed to persist strip args");
  }else{
    ESP_LOGI(TAG, "Persisted strip args to NVS");
  }
  free(key);
  return err;
}

void notify_status_update(int update_event){
  vTaskSuspendAll();
  xEventGroupSetBits(status_update_events, update_event);
  xEventGroupClearBits(status_update_events, update_event);
  xTaskResumeAll();
}

EventBits_t wait_for_status_update(int update_type){
  return xEventGroupWaitBits(status_update_events,update_type,0,pdFALSE,portMAX_DELAY);
}


void set_strip_args(uint strip_id, EffectArguments *args, bool from_sync){
  ESP_LOGI(TAG, "Set args for strip %u\n", strip_id);
  if(!from_sync && systemConfig -> strip_config[strip_id] -> sync_group_id != 0){
    sync_effect_arguments(args, systemConfig->strip_config[strip_id] -> sync_group_id);
  }
  strip_runners[strip_id] -> set_effect_arguments(args);
  persist_strip_args(strip_id, args);
  notify_status_update(EFFECT_ARGUMENTS_UPDATE);
}

esp_err_t validate_system_config(SystemConfig *config){
  if(config -> n_strip_config < 1 ){
    ESP_LOGE(TAG, "Config contains no strips");
    return ESP_FAIL;
  }
  return ESP_OK;
}

esp_err_t set_system_config(SystemConfig *newConfig){
  persist_config(newConfig);
  esp_restart();
  return ESP_FAIL;
}
InputConfig *generate_input_config(){
  InputConfig *config = (InputConfig*)malloc(sizeof(InputConfig));
  input_config__init(config);

  GpioInputConfig **gpioPtr = (GpioInputConfig **)malloc(sizeof(GpioInputConfig*) * 2);
  GpioInputConfig *gpioConfig = (GpioInputConfig*)malloc(sizeof(GpioInputConfig));

  GpioInputConfig *gpioConfig2 = (GpioInputConfig*)malloc(sizeof(GpioInputConfig));
  gpioPtr[0] = gpioConfig;
  gpioPtr[1] = gpioConfig2;
  gpio_input_config__init(gpioConfig);
  gpioConfig -> pin = 32;
  gpioConfig -> active_low = false;
  gpioConfig -> pull = true;

  gpio_input_config__init(gpioConfig2);
  gpioConfig2 -> pin = 33;
  gpioConfig2 -> active_low = false;
  gpioConfig2 -> pull = true;


  config -> gpio_inputs = gpioPtr;
  config -> n_gpio_inputs = 2;

  {InputAction *long_action = (InputAction*) malloc(sizeof(InputAction));
  input_action__init(long_action);
  gpioConfig -> long_action = long_action;
  gpioConfig2 -> long_action = long_action;
  long_action -> action_case = INPUT_ACTION__ACTION_ON_OFF;
  long_action -> on_off = (OnOffAction *)malloc(sizeof(OnOffAction));
  on_off_action__init(long_action -> on_off);
  InputAction *single_action = (InputAction*) malloc(sizeof(InputAction));
  input_action__init(single_action);
  gpioConfig -> single_action = single_action;
  single_action -> action_case = INPUT_ACTION__ACTION_COLOR;
  single_action -> color = (ColorAction *)malloc(sizeof(ColorAction));
  color_action__init(single_action -> color);}

  {InputAction *single_action2 = (InputAction*) malloc(sizeof(InputAction));
  input_action__init(single_action2);
  gpioConfig2 -> single_action = single_action2;
  single_action2 -> action_case = INPUT_ACTION__ACTION_COLOR;
  single_action2 -> color = (ColorAction *)malloc(sizeof(ColorAction));
  color_action__init(single_action2 -> color);
  single_action2 -> color -> backwards = true;}

  return config;
}



SystemConfig *generate_config(void){

  StripConfig* strip = (StripConfig*)malloc(sizeof(StripConfig));
  strip_config__init(strip);
  strip->strip_pin = 14;
  strip->leds = 4;
  strip->max_brightness = 100;
  strip->id= generate_uuid();


  StripConfig** strip_ptr = (StripConfig**)malloc(sizeof(StripConfig*));
  *strip_ptr = strip;

  SystemConfig *res =(SystemConfig*) malloc(sizeof(SystemConfig));
  system_config__init(res);
  res->device_name = (char*)"LEDongle2 Daiza";

  res->n_strip_config = 1;
  res->strip_config = strip_ptr;

  strip -> n_favorite_colors = 8;
  strip -> favorite_colors  = (uint32_t*) malloc(sizeof(uint32_t)* 7);
  strip -> favorite_colors[0] = RGB(0, 255, 0);
  strip -> favorite_colors[1] = RGB(0, 200, 90);
  strip -> favorite_colors[2] = RGB(120, 220, 0);
  strip -> favorite_colors[3] = RGB(220, 120, 0);
  strip -> favorite_colors[4] = RGB(180, 50, 140);
  strip -> favorite_colors[5] = RGB(255, 0, 0);
  strip -> favorite_colors[6] = RGB(0, 0, 255);
  strip -> favorite_colors[7] = RGB(100, 100, 100);
  //TODO
  //res -> n_supported_effects = num_effects;
  //res -> supported_effects = (char**)malloc(sizeof(char*) * num_effects);
  //for(uint8_t i = 0; i < res -> n_supported_effects; i++){
  //    res -> supported_effects[i] = (char*)effect_names[i];
  //}

  res -> wifi = (WifiConfig*)malloc(sizeof(WifiConfig));
  wifi_config__init(res->wifi);
  res -> wifi -> ssid = (char*)"leeklabs";
  res -> wifi -> password = (char*)"mikumiku39";
  res -> wifi -> connection_timeout = 10000;

  res -> updates = (UpdateConfig *)malloc(sizeof(UpdateConfig));
  update_config__init(res->updates);
  res -> updates -> auto_update = true;
  res -> updates -> update_url = (char*)"https://update.leeklabs.download/";
  //res -> updates -> update_server_cert = (char*)ca_cert_pem_start;

  //ESP_LOGI(TAG, "cert: %s", ca_cert_pem_start);
  res -> inputs = generate_input_config();
  res -> config_version = (char*)esp_ota_get_app_description() -> version;
  return res;
}

EffectArguments *generate_effect_arguments(){

  SolidArguments *effect = (SolidArguments*)malloc(sizeof(SolidArguments));
  solid_arguments__init(effect);
  effect -> color = RGB(0, 255, 255);

  EffectArguments *args = (EffectArguments*)malloc(sizeof(EffectArguments));
  effect_arguments__init(args);
  args -> brightness = 100;
  args -> power_state = true;

  EffectState *state = (EffectState*)malloc(sizeof(EffectState));
  effect_state__init(state);

  args -> effect_arguments_case = EFFECT_ARGUMENTS__EFFECT_ARGUMENTS_SOLID_ARGUMENTS;
  args -> solidarguments = effect;
  return args;
}

EffectArguments *restore_effect_args(uint strip_id, nvs_handle handle){


  esp_err_t err = ESP_OK;
  size_t required_size = 0;  // value will default to 0, if not set yet in NVS
  // obtain required memory space to store blob being read from NVS
  char *key = (char*)malloc(8);
  snprintf(key,8, "args%u", strip_id);

  err = nvs_get_blob(handle, key, NULL, &required_size);
  if (err != ESP_OK && err != ESP_ERR_NVS_NOT_FOUND) return NULL;
  if (required_size == 0) {
    free(key);
    return NULL;
  }else{
    uint8_t *buf = (uint8_t*)malloc(required_size);
    nvs_get_blob(handle, key, buf, &required_size);
    EffectArguments *res = (EffectArguments *)malloc(sizeof(EffectArguments));
    res = effect_arguments__unpack(NULL, required_size, buf);

    res -> power_state = true;
    free(key);
    return res;
  }
}

esp_err_t initialize_strip_config(nvs_handle handle){
  ESP_LOGI(TAG, "Initializing %d LED strips", systemConfig ->n_strip_config);
  esp_err_t err = ESP_OK;
  strip_runners = (WS2812Runner **)malloc(sizeof(WS2812Runner *) *  systemConfig -> n_strip_config);
  if(strip_runners == NULL) return ESP_FAIL;
  for(int i = 0; i < systemConfig -> n_strip_config; i++){
    ESP_LOGI(TAG, "Initializing strip %d", i);

    StripConfig *stripConfig = systemConfig -> strip_config[i];
    stripConfig -> current_favorite_color = -1;
    if(stripConfig -> sync_group_name == NULL || strlen(stripConfig -> sync_group_name) == 0){
      stripConfig -> sync_group_id = 0;
    }else{
      char hash[16];
      md5hash((uint8_t*)stripConfig -> sync_group_name, strlen(stripConfig -> sync_group_name), (uint8_t *)hash);
      for(int i = 4; i < 16; i += 4){
        for(int j = 0; j < 4; j++){
          hash[j] ^= hash[i+j];
        }
      }
      stripConfig -> sync_group_id = (((uint32_t) hash[0]) << 24) | (((uint32_t) hash[1]) << 16) | (((uint32_t) hash[2]) << 8) | (((uint32_t) hash[3]));
    }
    EffectArguments *args = restore_effect_args(i, handle);
    if(args == NULL){
      args = generate_effect_arguments();
    }
    WS2812_strip* strip = new WS2812_dled_strip(stripConfig->strip_pin, stripConfig->leds, stripConfig->max_brightness, i);
    strip_runners[i] = new WS2812Runner(strip, stripConfig, args);

  }
  return err;
}

void update_system_status(SystemStatus* new_status){
  SystemStatus *old_status = systemStatus;
  systemStatus = new_status;
  if(old_status != new_status){
    system_status__free_unpacked(old_status, NULL);
  }
  notify_status_update(SYSTEM_STATUS_UPDATE);
}

SystemStatus *initialize_system_status(void){

  SystemStatus *res = (SystemStatus *)malloc(sizeof(SystemStatus));
  system_status__init(res);
  const char *fw_version = esp_ota_get_app_description() -> version;

  //size_t version_length = strnlen(fw_version, 32)+1;
  //char *fw_version_writable = (char *)malloc(version_length);
  //strncpy(fw_version_writable, fw_version, 32);
  res->firmware_version = (char*)fw_version;
  res -> wifi_state = WIFI_STATE__UNINITIALIZED;

  //TODO supported effects
  return res;
}

void upgrade_system_config(SystemConfig* old_config){
  if(old_config -> config_version == NULL || !strncmp(old_config->config_version, esp_ota_get_app_description() -> version, VERSION_STRING_LENGTH)){
    ESP_LOGI(TAG, "Updating config");
    //Remove server cert from config because large BT stuff doesn't work yet
    if(old_config -> updates != NULL && old_config -> updates -> update_server_cert != NULL){
      old_config -> updates -> update_server_cert = NULL;
    }
    old_config -> config_version =  (char*)esp_ota_get_app_description() -> version;
    persist_config(old_config);//this reboots the system so it will never return
  }
}

esp_err_t reset_config(){
  return nvs_flash_erase();
}

esp_err_t initialize_system_config(void){
  esp_err_t err = nvs_flash_init();
  if (err == ESP_ERR_NVS_NO_FREE_PAGES /*|| err == ESP_ERR_NVS_NEW_VERSION_FOUND*/) {
    // NVS partition was truncated and needs to be erased
    // Retry nvs_flash_init
    ESP_ERROR_CHECK(nvs_flash_erase());
    err = nvs_flash_init();
    if(err != ESP_OK){
      ESP_LOGE(TAG, "FAILED TO READ AND REFORMAT NVS %d", err);
      return err;
    }
  }


  status_update_events = xEventGroupCreate();

  // Open
  nvs_handle handle;
  err = nvs_open(CONFIGURATION_STORAGE_NAMESPACE, NVS_READWRITE, &handle);
  if (err != ESP_OK) return err;

  heap_caps_check_integrity_all(true);
  systemStatus = initialize_system_status();
  heap_caps_check_integrity_all(true);

  size_t required_size = 0;  // value will default to 0, if not set yet in NVS
  // obtain required memory space to store blob being read from NVS
  heap_caps_check_integrity_all(true);
  err = nvs_get_blob(handle, CONFIG_NVS_KEY, NULL, &required_size);
  if (err != ESP_OK && err != ESP_ERR_NVS_NOT_FOUND) return err;
  heap_caps_check_integrity_all(true);
  if (required_size == 0) {
    ESP_LOGI(TAG, "No config found, generating...");
    systemConfig = generate_config();
    persist_config(systemConfig);
  } else {
    void* config_protobuf = malloc(required_size);
    if(config_protobuf==NULL) ESP_ERROR_CHECK(-1);
    err = nvs_get_blob(handle, CONFIG_NVS_KEY, config_protobuf, &required_size);
    if (err != ESP_OK) {
      ESP_LOGE(TAG, "Failed to read config from nvs %s", esp_err_to_name(err));
      free(config_protobuf);
      nvs_commit(handle);
      nvs_close(handle);
      return err;
    }
    systemConfig = system_config__unpack(NULL, required_size, (uint8_t*)config_protobuf);
    if (systemConfig == NULL)
      {
        ESP_LOGE(TAG, "error unpacking config read from nvs");
        systemConfig = generate_config();
        persist_config(systemConfig);
      }
    ESP_LOGI(TAG, "System config loaded from NVM");
    //TODO validate config and delete if wrong
    initialize_strip_config(handle);
    input_init(systemConfig -> inputs);
  }


  if(systemConfig==NULL){
    ESP_LOGE(TAG, "Config was not initialized");
    nvs_commit(handle);
    nvs_close(handle);
    return ESP_FAIL;
  }
  upgrade_system_config(systemConfig);
  nvs_commit(handle);
  nvs_close(handle);
  return ESP_OK;
}

void strip_next_color(uint strip_index, bool backwards){
  StripConfig* strip = systemConfig -> strip_config[strip_index];

  if(backwards){
    if(strip -> current_favorite_color <= 0){
      strip -> current_favorite_color = (strip -> n_favorite_colors - 1);
    }else{
      strip -> current_favorite_color = strip -> current_favorite_color - 1;
    }
  }else{
    if(strip -> current_favorite_color == strip -> n_favorite_colors - 1 || strip -> current_favorite_color == -1){
      strip -> current_favorite_color = 0;
    }else{
      strip -> current_favorite_color = strip -> current_favorite_color + 1;
      }
  }
  uint32_t color = strip -> favorite_colors[strip -> current_favorite_color];

  persist_config(systemConfig);
  EffectArguments *args = strip_runners[strip_index] -> get_effect_arguments();
  set_effect_color(args, color);
  set_strip_args(strip_index, args, false);
}

void toggle_strip(int i, bool state){
  EffectArguments *args = strip_runners[i] -> get_effect_arguments();
  args -> power_state = state;
  set_strip_args(i, args, false);
}
