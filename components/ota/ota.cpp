#include "ota.h"
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <system_config.h>
#include <ledongle_wifi.h>
#include <esp_http_client.h>
#include "esp_https_ota.h"
#include <alloca.h>

#define OTA_CHECK_UPDATE 1
#define OTA_START_UPDATE 2


static TaskHandle_t ota_task;
static const char *TAG = "OTA";

extern const uint8_t ca_cert_pem_start[] asm("_binary_ca_cert_pem_start");
extern const uint8_t ca_cert_pem_end[]   asm("_binary_ca_cert_pem_end");

static bool connect_to_wifi(void){
  ESP_LOGI(TAG, "Checking wifi connection");
  if(systemStatus -> wifi_state == WIFI_STATE__CONNECTED){
    return true;
  }
  if(systemStatus -> wifi_state != WIFI_STATE__CONNECTING){
    wifi_connect();
  }
  while(systemStatus -> wifi_state != WIFI_STATE__CONNECTED){
    wait_for_status_update(SYSTEM_STATUS_UPDATE);
    if(systemStatus -> wifi_state == WIFI_STATE__FAILED){
      return false;
    }
  }
  return true;
}

static bool check_ota_update(){
  heap_caps_check_integrity_all(true);

  ESP_LOGI(TAG, "Checking for updates");
  if(!connect_to_wifi()) return false;

  esp_http_client_config_t config = {};
  char *url = (char*)malloc(strlen(systemConfig->updates->update_url) +  strlen("version_list") + 1);
  url[0] = 0;
  strcat(url, systemConfig->updates->update_url);
  strcat(url, "version_list");
  ESP_LOGI(TAG, "%s", url);
  config.url = url;
  config.cert_pem =(const char*) ca_cert_pem_start;
  heap_caps_check_integrity_all(true);

    esp_http_client_handle_t client = esp_http_client_init(&config);
    esp_err_t err = esp_http_client_perform(client);
    ESP_LOGI(TAG, "status %d", esp_http_client_get_status_code(client));
    if (err == ESP_OK) {
      uint32_t current_version_index = 0xffffff;
      uint32_t i = 0;
      char new_version[VERSION_STRING_LENGTH +1];
      while(esp_http_client_read(client, new_version, VERSION_STRING_LENGTH +1) >= VERSION_STRING_LENGTH){
        ESP_LOGI(TAG, "%.7s", new_version);
        i++;
        heap_caps_check_integrity_all(true);

        if(!strncmp(new_version, systemStatus -> firmware_version, VERSION_STRING_LENGTH)){
          current_version_index = i;
        }
        if(current_version_index == i-1) break;//we found the new revision

      }
      if(current_version_index >= i){
        ESP_LOGI(TAG, "Running latest FW");
        return false;
      }else{
        ESP_LOGI(TAG, "New fw revision %.*s available",VERSION_STRING_LENGTH, new_version);
        char *next_version = (char*)malloc(VERSION_STRING_LENGTH + 1);
        strncpy(next_version, new_version, VERSION_STRING_LENGTH);
        next_version[VERSION_STRING_LENGTH] = 0;
        systemStatus -> update_next_version = next_version;
        return true;
      }
    }else{
      return false;
    }
    esp_http_client_cleanup(client);

  return false;
}

static void execute_ota_update(){
  if(!connect_to_wifi()) return;
  esp_http_client_config_t config = {};
  char *url = (char*)alloca(strlen(systemConfig->updates->update_url) + VERSION_STRING_LENGTH + strlen(".bin") + 1);
  url[0] = 0;
  strcat(url, systemConfig->updates->update_url);
  strcat(url, systemStatus -> update_next_version);
  strcat(url, ".bin");
  config.url = url;
  config.cert_pem = (const char*)ca_cert_pem_start;

  esp_err_t ret = esp_https_ota(&config);
  if (ret == ESP_OK) {
    esp_restart();
  } else {
    ESP_LOGE(TAG, "Firmware upgrade failed");
  }

}

void ota_task_runner(void *nothing){
  uint32_t notification_value = 0;
  for(;;){
    xTaskNotifyWait(0, 0, &notification_value, portMAX_DELAY);
    switch(notification_value){
    case OTA_CHECK_UPDATE:
      if(check_ota_update()){
        systemStatus->update_status = UPDATE_STATUS__UPDATE_AVAILABLE;
        update_system_status(systemStatus);
        if(systemConfig->updates->auto_update){
          ota_start_update();
        }
      }
      break;
    case OTA_START_UPDATE:
      systemStatus -> update_status = UPDATE_STATUS__UPDATING;
      update_system_status(systemStatus);
      execute_ota_update();
      systemStatus -> update_status = UPDATE_STATUS__UPDATE_FAILED;
      update_system_status(systemStatus);
      break;
    default:
      ESP_LOGW(TAG, "Unrecognized notification value %u in OTA task", notification_value);
      break;
    }
  }
}

esp_err_t ota_init(){
  xTaskCreate(ota_task_runner, "OTA", 8192, NULL, 10, &ota_task);
  ota_check_for_update();
  return ESP_OK;
}

void ota_check_for_update(){
  ESP_LOGI(TAG, "Checking for system update");
  xTaskNotify(ota_task, OTA_CHECK_UPDATE,  eSetValueWithOverwrite );
}

void ota_start_update(){
  ESP_LOGI(TAG, "Initiating system update");
  xTaskNotify(ota_task, OTA_START_UPDATE,  eSetValueWithOverwrite );
}
