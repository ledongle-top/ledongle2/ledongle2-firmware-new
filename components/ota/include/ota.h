#pragma once
#include <esp_err.h>
#define VERSION_STRING_LENGTH 7


esp_err_t ota_init();

void ota_check_for_update();

void ota_start_update();
