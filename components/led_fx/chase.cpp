#include "chase.h"

void chase(WS2812_strip* strip, ChaseArguments *args, ChaseState* state){
  int distance = args -> distance <= -1 ? 3 : (args -> distance/10 + 1);
  int speed = args -> speed <= -1 ? 30 : args -> speed;

  if(state -> frame_count >= 50){
    strip -> set_all_leds(0,0,0);
    for(int i = state -> position; i < strip -> num; i += distance){
      strip -> set_led(i,R(args -> color),G(args -> color),B(args -> color));
    }
    state -> position++;
    if(state -> position >= distance) {
      state -> position = 0;
      if(args -> color & RANDOM_COLOR_BIT) args -> color = random_color();
    }
    state -> frame_count = 0;
  }
  state -> frame_count += (speed/5)+1;
}
