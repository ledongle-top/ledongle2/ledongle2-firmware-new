#include "rainbow.h"
#include <stdio.h>
void rainbow(WS2812_strip* strip, RainbowArguments *args, RainbowState* state){
  printf("%u\n", state->hue);
  uint32_t color = hsv_to_rgb(state->hue >> 8, 255, 255);
  strip -> set_all_leds(R(color), G(color), B(color));
  state -> hue = (state -> hue + ((args->speed == -1)? 50 : (args -> speed+1))) % (360<<8);
}
