#include "strobe.h"

void strobe(WS2812_strip* strip, StrobeArguments *args, StrobeState* state){

  if(state -> frame_count < 50){
    strip -> set_all_leds(0,0,0);
  }else{
    if(args -> color & RANDOM_COLOR_BIT){
      args -> color = random_color();
    }
    strip -> set_all_leds(R(args -> color),G(args -> color),B(args -> color));
  }
  int speed = args -> speed == -1 ? 30 : args -> speed;
  state -> frame_count += (speed/5)+1;
  if(state->frame_count >= 100) state -> frame_count = 0;
}
