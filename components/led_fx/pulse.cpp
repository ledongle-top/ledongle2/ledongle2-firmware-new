#include "pulse.h"

void pulse(WS2812_strip* strip, PulseArguments *args, PulseState* state){
  int speed = args -> speed == -1 ? 50 : args -> speed;
  if(state -> frame_count <= 255){//pulse down
    int frame_count = state -> frame_count;

    uint8_t r = (((int)R(args -> color)) << 8) / (frame_count+1);
    uint8_t g = (((int)G(args -> color)) << 8) / (frame_count+1);
    uint8_t b = (((int)B(args -> color)) << 8) / (frame_count+1);
    strip -> set_all_leds(r,g,b);
  }else{//pulse up
    int frame_count = state -> frame_count;
    uint8_t r = (((int)R(args -> color)) << 8) / (512 - frame_count);
    uint8_t g = (((int)G(args -> color)) << 8) / (512 - frame_count);
    uint8_t b = (((int)B(args -> color)) << 8) / (512 - frame_count);
    strip -> set_all_leds(r,g,b);
 }
  state -> frame_count += (speed/5)+1;
  if(state->frame_count >= 512){
    if(args -> color & RANDOM_COLOR_BIT) args->color = random_color();
    state -> frame_count = 0;
  }
}
