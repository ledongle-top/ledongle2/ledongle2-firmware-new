#include "solid.h"
#include <stdlib.h>
#include "stdio.h"
static int max(int a, int b){
  return ((a > b) ? a : b);
}

void solid(WS2812_strip *strip, SolidArguments *args, SolidState *state){
  for(uint16_t i = 0; i < strip -> num; i++){
    uint32_t current_color = strip -> get_led(i);
      uint8_t current_r = R(current_color);
      uint8_t current_g = G(current_color);
      uint8_t current_b = B(current_color);

      uint8_t target_r = R(args->color);
      uint8_t target_g = G(args->color);
      uint8_t target_b = B(args->color);

      if(current_r > target_r){
          current_r -= max(1, (current_r - target_r)/2 );
      }else if (current_r < target_r){
          current_r += max(1, (target_r - current_r )/2);
      }

      if(current_g > target_g){
        current_g -= max(1, (current_g - target_g) /2);
      }else if(current_g < target_g){
          current_g += max(1, (target_g - current_g ) /2);
      }

      if(current_b > target_b){
        current_b -= max(1, (current_b - target_b) /2);
      }else if(current_b < target_b){
        current_b += (max(1, (target_b - current_b ) /2));
      }
      strip -> set_led(i, current_r, current_g, current_b);
  }
}
