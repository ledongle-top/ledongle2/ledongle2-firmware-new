#include "rainbow_cycle.h"

void rainbow_cycle(WS2812_strip* strip, RainbowArguments *args, RainbowState* state){
  unsigned hue_shifted = state -> hue << 8;
  unsigned hue_step = (360 << 8) / strip -> num;
  state -> hue += args->speed+1;
  for(unsigned i = 0; i < strip -> num; i++){
    hue_shifted = (hue_shifted + hue_step) % (360>>8);
    uint32_t color = hsv_to_rgb(hue_shifted >> 8, 255, 255);
    strip -> set_led(i, R(color), G(color), B(color));
  }
}
