#include "blinksync.h"
#include <esp_now.h>
#include <esp_log.h>
#include "esp_wifi.h"
#include "nvs_flash.h"
#include "esp_event_loop.h"
#include "tcpip_adapter.h"
#include "esp_wifi.h"
#include "esp_log.h"
#include "esp_system.h"
#include "esp_now.h"
#include "rom/ets_sys.h"
#include "rom/crc.h"
#include "now_message.pb-c.h"

uint8_t broadcast_mac[] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff};

static const char *TAG = "SYNC";

static void receive_callback(const uint8_t *sender_mac, const uint8_t *data, int len){
  ESP_LOGI(TAG, "RX %d", len);
  NowMessage *message = now_message__unpack(NULL, len, data);
  if(message -> arguments_sync != NULL){
    if(message -> arguments_sync -> arguments != NULL){
    for(int i = 0; i < systemConfig -> n_strip_config; i++){
      if(systemConfig -> strip_config[i] -> sync_group_id == message -> arguments_sync -> group_id){
        set_strip_args(i, message -> arguments_sync -> arguments, true);
      }
    }
    }
  }
  if(message -> state_sync != NULL){
  }
}

esp_err_t sync_init(void){
  ESP_LOGI(TAG, "Initializing sync");
  esp_err_t err = esp_now_init();
  esp_now_register_recv_cb(receive_callback);
  /* Set primary master key. */
  ESP_ERROR_CHECK( esp_now_set_pmk((uint8_t *)"mikumiku39393939") );

  /* Add broadcast peer information to peer list. */
  esp_now_peer_info_t *peer = (esp_now_peer_info_t*)malloc(sizeof(esp_now_peer_info_t));
  if (peer == NULL) {
    ESP_LOGE(TAG, "Malloc peer information fail");
    esp_now_deinit();
    return ESP_FAIL;
  }
  memset(peer, 0, sizeof(esp_now_peer_info_t));
  peer->channel = 13;
  peer->ifidx = ESP_IF_WIFI_STA;
  peer->encrypt = false;
  memcpy(peer->peer_addr, broadcast_mac, ESP_NOW_ETH_ALEN);
  ESP_ERROR_CHECK( esp_now_add_peer(peer) );
  free(peer);

  return err;
}

esp_err_t sync_effect_arguments(EffectArguments *args, uint32_t group_id){
  NowMessage message = NOW_MESSAGE__INIT;
  ArgumentsSync sync = ARGUMENTS_SYNC__INIT;
  sync.group_id = group_id;
  sync.arguments = args;
  message.arguments_sync = &sync;

  size_t msg_size = now_message__get_packed_size(&message);
  ESP_LOGD(TAG, "Syncing args, size: %u", msg_size);
  uint8_t *buf = (uint8_t*) malloc(msg_size);
  if(buf == NULL) ESP_ERROR_CHECK(ESP_FAIL);
  now_message__pack(&message, buf);
  return esp_now_send(broadcast_mac, buf, msg_size);
}
