#pragma once
#include <system_config.h>
#include <effect_arguments.pb-c.h>

esp_err_t sync_init(void);

esp_err_t sync_effect_arguments(EffectArguments* args, uint32_t group_id);
