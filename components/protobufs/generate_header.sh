#!/bin/bash

protoarrays=$1/c_out/effects/protos.cpp
protoheader=$1/c_out/protos.h
effectselector=$1/c_out/effect_selector.cblock
all_effects_header=$1/c_out/effects.h
effectstateselector=$1/codeblocks/effect_state_selector.cblock
colorsetter=$1/c_out/effect_color_setter.cblock

echo "#pragma once" > $protoheader
echo "#include \"proto_descriptor.h\"" >> $protoheader
echo "extern const char protos[];" >> $protoheader
echo "extern int num_protos;" >> $protoheader

array="const char protos[]=\""
num_protos=0

echo "" > $effectselector
echo "" > $all_effects_header
echo "" > $effectstateselector
echo "" > $colorsetter
echo "#include \"protos.h\"" > $protoarrays


for f in $1/effects/*.proto; do

    num_protos=$((num_protos + 1));

    IFS= read -r -d '' content <$f
    content=${content//'"'/'\"'}
    content=${content//$'\n'/'\n'}
    name=${f%%.proto}
    name=${name##*/}
    array+="${content}"

    echo "case EFFECT_ARGUMENTS__EFFECT_ARGUMENTS_${name^^}_ARGUMENTS:" >> $colorsetter
    if [[ $content == *"uint32 color "* ]];then
        echo "args -> ${name}arguments -> color = color;" >> $colorsetter
    fi
    echo "return;" >> $colorsetter

    echo "case EFFECT_ARGUMENTS__EFFECT_ARGUMENTS_${name^^}_ARGUMENTS:" >> $effectselector
    echo "return _${name}_wrapper;" >> $effectselector

    echo "case EFFECT_ARGUMENTS__EFFECT_ARGUMENTS_${name^^}_ARGUMENTS:" >> $effectstateselector
    echo "res = malloc(sizeof(${name^}State));" >> $effectstateselector
    echo "${name}_state__init((${name^}State*)res);" >> $effectstateselector
    echo "effect_state -> ${name}state = (${name^}State*)res;" >> $effectstateselector
    echo "break;" >> $effectstateselector

    effectheader="$1/c_out/${name}.h"
    effectcpp="$1/c_out/${name}_wrapper.cpp"
    echo "#include \"WS2812_strip.h\"" > ${effectheader}
    echo "#include \"WS2812_effects.h\"" >> ${effectheader}
    echo "#include \"effect_arguments.pb-c.h\"" >> ${effectheader}
    echo "#include \"color.h\"" >> ${effectheader}
    echo "void ${name}(WS2812_strip* strip, ${name^}Arguments *effect_args, ${name^}State *effect_state);" >> ${effectheader}
    echo "void _${name}_wrapper(WS2812_strip* strip, EffectArguments *effect_args, EffectState *effect_state);" >> ${effectheader}

    echo "#include \"WS2812_strip.h\"" > ${effectcpp}
    echo "#include \"WS2812_effects.h\"" >> ${effectcpp}
    echo "#include \"color.h\"" >> ${effectcpp}
    echo "#include \"WS2812_effects.h\"" >> ${effectcpp}
    echo "#include \"${name}.h\""  >> ${effectcpp}
    echo "void _${name}_wrapper (WS2812_strip* strip, EffectArguments *effect_args, EffectState *effect_state){" >> ${effectcpp}
    echo "${name^}Arguments *args = effect_args -> ${name}arguments;" >> ${effectcpp}
    echo "${name^}State *state = effect_state -> ${name}state;">> ${effectcpp}
    echo "return ${name}(strip, args, state);" >> ${effectcpp}
    echo "}" >> ${effectcpp}

    echo "#include \"${name}.h\"" >> $all_effects_header
done


for f in $1/*.proto; do
    num_protos=$((num_protos + 1));
    IFS= read -r -d '' content <$f
    content=${content//'"'/'\"'}
    content=${content//$'\n'/'\n'}
    name=${f%%.proto}
    name=${name##*/}
    array+="${content}"
done


array="${array}\";"


echo $array >> $protoarrays
echo $name_array >> $protoarrays
echo "int num_protos = ${num_protos};" >> $protoarrays
