#include "input.h"
#include "gpio_input.h"


esp_err_t input_init(InputConfig *input){
  if(input == NULL){
    return ESP_OK;
  }
  esp_err_t err = ESP_OK;
  gpio_inputs_init(input->gpio_inputs, input->n_gpio_inputs);
  return err;
}
