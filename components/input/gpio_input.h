#pragma once
#include "input.h"
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <stdio.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "driver/gpio.h"
#include "esp_log.h"

esp_err_t gpio_inputs_init(GpioInputConfig **configs, int num);
