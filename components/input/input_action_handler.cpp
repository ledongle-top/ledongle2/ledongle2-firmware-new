#include "input_action_handler.h"
#include <effect_color_setter.h>
#define ALL_STRIPS -1

static const char *TAG = "INHANDLER";


static void onOff(InputAction* action){
  if(action -> strip == ALL_STRIPS){
  for(int i = 0; i < systemConfig -> n_strip_config; i++){
    toggle_strip(i, action -> on_off -> state);
  }
  }else{
    toggle_strip(action -> strip, action -> on_off -> state);
  }
  action -> on_off -> state = !action -> on_off -> state;
}


static void color_action(InputAction *action){
  strip_next_color(action -> strip, action -> color -> backwards);
}

void handle_input_action(InputAction *action){
  if(action == NULL){
    ESP_LOGI(TAG, "Undefined action");
    return;
  }
  switch(action -> action_case){
  case INPUT_ACTION__ACTION_COLOR:
    color_action(action);
    break;
  case INPUT_ACTION__ACTION_ON_OFF:
    onOff(action);
    break;
  case INPUT_ACTION__ACTION_EFFECT:
    break;
  default:
    ESP_LOGW(TAG, "Unhandled action %d", action->action_case);
    return;
  }
  persist_config(systemConfig);
}
