#include "gpio_input.h"
#include "system_config.h"
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <stdio.h>
#include <millis.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "driver/gpio.h"
#include "esp_log.h"
#include "input_action_handler.h"

#define PIN_BIT(x) (1ULL<<x)
#define BUTTON_DOWN (1)
#define BUTTON_UP (2)
#define LONG_PRESS_DURATION (1000)

typedef struct {
	uint8_t pin;
    uint8_t event;
} button_event_t;

typedef struct {
	gpio_num_t pin;
    bool inverted;
	uint16_t history;
    uint64_t down_time;
} debounce_t;


static const char* TAG = "GPIO";


int pin_count = -1;
debounce_t * debounce;

static void update_button(debounce_t *d) {
    d->history = (d->history << 1) | (gpio_get_level(d->pin) ^  d-> inverted);
}

#define MASK   0b1111000000111111
static bool button_rose(debounce_t *d) {
    if ((d->history & MASK) == 0b0000000000111111) {
        d->history = 0xffff;
        return 1;
    }
    return 0;
}
static bool button_fell(debounce_t *d) {
    if ((d->history & MASK) == 0b1111000000000000) {
        d->history = 0x0000;
        return 1;
    }
    return 0;
}
static bool button_down(debounce_t *d) {
    //if (d->inverted) return button_fell(d);
    return button_rose(d);
}
static bool button_up(debounce_t *d) {
    //if (d->inverted) return button_rose(d);
    return button_fell(d);
}





//TODO
static void button_task(void *pvParameter)
{
    while (1) {
        for (int idx=0; idx<pin_count; idx++) {
            update_button(&debounce[idx]);
            if (debounce[idx].down_time && (millis() - debounce[idx].down_time > LONG_PRESS_DURATION)) {
                debounce[idx].down_time = 0;
                ESP_LOGI(TAG, "%d LONG", debounce[idx].pin);
                handle_input_action(systemConfig -> inputs -> gpio_inputs[idx] -> long_action);
            } else if (button_down(&debounce[idx])) {
                debounce[idx].down_time = millis();
                ESP_LOGI(TAG, "%d DOWN", debounce[idx].pin);
            } else if (button_up(&debounce[idx])) {
                if(debounce[idx].down_time != 0){
                    ESP_LOGI(TAG, "%d SHORT", debounce[idx].pin);
                    handle_input_action(systemConfig -> inputs -> gpio_inputs[idx] -> single_action);
                }
                debounce[idx].down_time = 0;
            }
        }
        vTaskDelay(10/portTICK_PERIOD_MS);
    }
}


esp_err_t gpio_inputs_init(GpioInputConfig **configs, int num){
    esp_err_t err = ESP_OK;
    ESP_LOGI(TAG, "Setting up %d gpio inputs", num);

    if (pin_count != -1) {
        ESP_LOGW(TAG, "Already initialized");
        return ESP_FAIL;
    }

    pin_count = num;
    debounce = (debounce_t *)calloc(pin_count, sizeof(debounce_t));

    if(num == 0) return ESP_OK;

    uint64_t pin_mask = 0;
    for(int i = 0; i < num; i++){
        pin_mask |= PIN_BIT(configs[i]->pin);
        gpio_num_t pin = (gpio_num_t)configs[i] -> pin;
        gpio_set_direction(pin, GPIO_MODE_INPUT);
        if(configs[i] -> pull){
            gpio_set_pull_mode(pin, configs[i] -> active_low ?   GPIO_PULLUP_ONLY : GPIO_PULLDOWN_ONLY);
        }
        ESP_LOGI(TAG, "Registering button input: %d", pin);
        debounce[i].pin = pin;
        debounce[i].down_time = 0;
        debounce[i].inverted = configs[i] -> active_low;
    }

    // Spawn a task to monitor the pins
    xTaskCreate(&button_task, "button_task", 4096, NULL, 10, NULL);





    return err;
}
