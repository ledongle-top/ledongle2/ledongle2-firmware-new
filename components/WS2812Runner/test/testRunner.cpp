#include <unity.h>
#include "WS2812Runner.h"
#include "WS2812Dummy.h"

TEST_CASE("Test WS2812Runner command receive", "[WS2812Runner]"){
  /*  WS2812Dummy *strip = new WS2812Dummy(1);
  printf("creating runner\n");
  WS2812Runner *runner =  new WS2812Runner(strip);

  printf("creating request\n");
  auto request = std::make_shared<RequestT>();
  request->id = 39;

  RequestPayloadUnion payload;

  StripSetEffectRequestT effectRequest;
  StripEffectUnion effectUnion;
  EffectSolidT effect;
    //effect.r = 0;
    //effect.g = 255;
    //effect.b = 255;
  effectUnion.Set(std::move(effect));
  effectRequest.effect = effectUnion;

  payload.Set(std::move(effectRequest));

  request->payload = payload;

  printf("sending request");
  std::shared_ptr<Future<std::shared_ptr<ReplyT>>> replyFuture = runner->receive(request);
  std::shared_ptr<ReplyT> reply;
  printf("waiting for reply\n");
  TEST_ASSERT_EQUAL(ESP_OK,replyFuture->await(reply, pdMS_TO_TICKS(4000)));
  printf("checking reply\n");
  TEST_ASSERT_EQUAL(StatusCode_OK,reply->status);

  StripGetEffectRequestT getRequest;
  payload.Set(std::move(getRequest));
  std::shared_ptr<Future<std::shared_ptr<ReplyT>>> getReplyFuture = runner->receive(request);
  TEST_ASSERT_EQUAL(ESP_OK,getReplyFuture->await(reply, pdMS_TO_TICKS(4000)));
  TEST_ASSERT_NOT_NULL(reply->payload.AsStripStateReply());
  TEST_ASSERT_EQUAL(ReplyPayload_StripStateReply, reply->payload.AsStripStateReply()->effect.type);
  vTaskDelay(pdMS_TO_TICKS(4000));*/
}

TEST_CASE("Test WS2812Runner command execution", "[WS2812Runner]"){
  WS2812RMT_strip strip = WS2812RMT_strip(27, 10);
  printf("creating runner\n");
  WS2812Runner runner =  WS2812Runner(&strip);

  printf("creating request\n");
  auto request = RequestT();
  request.id = 39;

  RequestPayloadUnion payload;
  auto rainbow = EffectRainbowT();
  StripEffectUnion effect;
  effect.Set(std::move(rainbow));
  auto stripState = StripStateT();
  stripState.effect = effect;
  payload.Set(std::move(stripState));
  request.payload = payload;
  printf("sending request");
  flatbuffers::FlatBufferBuilder builder;
  auto offset = CreateRequest(builder, &request);
  builder.Finish(offset);
  size_t size = 0;
  size_t msg_offset = 0;
  auto buffer = builder.ReleaseRaw( size, msg_offset);
  printf("offset %u size %u", msg_offset, size);

  flatbuffers::Verifier verifier(buffer+msg_offset,size);
  void *message = malloc(size);
  memcpy(message, buffer+msg_offset, size);
  //free(buffer);
  TEST_ASSERT(VerifyRequestBuffer(verifier));
  runner.receive(message,size);

  //TEST_ASSERT_EQUAL(ESP_OK,replyFuture->await(reply, pdMS_TO_TICKS(4000)));
  //printf("checking reply\n");
  //TEST_ASSERT_EQUAL(StatusCode_OK,reply->status);
  //vTaskDelay(pdMS_TO_TICKS(400));
  //TEST_ASSERT_EQUAL(255, strip.buffer[1]);
  vTaskDelay(pdMS_TO_TICKS(40000));
}
