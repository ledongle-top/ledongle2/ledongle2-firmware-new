#ifndef WS2812RUNNER_H
#define WS2812RUNNER_H
#include "WS2812RMT.h"
#include "WS2812_effects.h"
#include "WS2812_strip.h"
#include <esp_err.h>
#include <esp_log.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/queue.h>
#include <nvs_flash.h>
#include "solid.pb-c.h"
#include "strip_config.pb-c.h"
#include "effect_arguments.pb-c.h"

class WS2812Runner{
 private:
  WS2812_strip *strip;
  TaskHandle_t taskHandle;
  static void WS2812RunnerFunc(void *runnerPtr);
  int executeEffect(void);
  //effects
  uint8_t brightness = 255;
  void stop(void);
  StripConfig* config;
  EffectArguments* effect_arguments;
  EffectState* effect_state;
public:
  WS2812Runner(WS2812_strip *strip, StripConfig* config, EffectArguments *args);
  ~WS2812Runner();
  void set_effect_arguments(EffectArguments* args);
  void wake(void);
  EffectArguments* get_effect_arguments(void);
};
#endif
