#include "WS2812Runner.h"
#include <errno.h>
#include <color.h>
#include "WS2812_effects.h"
#include "effect_selector.h"

static const char* TAG = "WS2812RUNNER";


EffectArguments* WS2812Runner::get_effect_arguments(){
  return effect_arguments;
}

WS2812Runner::WS2812Runner(WS2812_strip* strip, StripConfig* config, EffectArguments *args):   strip(strip), config(config), effect_arguments(args){
  ESP_LOGI(TAG, "Initializing runner");
  effect_state = (EffectState *) malloc(sizeof(EffectState));
  effect_state__init(effect_state);
  generate_effect_state(args, effect_state);
  xTaskCreatePinnedToCore(WS2812RunnerFunc,
                          "",
                          4096,
                          this,
                          CONFIG_WS2812_RUNNER_TASK_PRIORITY,
                          &this->taskHandle,
                          CONFIG_WS2812_RUNNER_TASK_CORE);
}

void WS2812Runner::WS2812RunnerFunc(void *runnerPtr){
  WS2812Runner *runner = (WS2812Runner*) runnerPtr;
  for(;;){
    //render a frame and get a time until the next one
    int delay_ms = runner->executeEffect();
    runner->strip->showFrame();

    TickType_t delay_ticks = delay_ms == INFINITEDELAY ? portMAX_DELAY : pdMS_TO_TICKS(delay_ms);//Wait forever if there are no more frames to render (i.e. it's a static effect)

    if(xTaskNotifyWait( 0,0, NULL, delay_ticks )){
        //state has changed, go apply it TODO
    }
  }
}


uint8_t adjustBrightness(uint8_t color, uint8_t brightness){
  if(brightness == 255) return color;
  uint16_t multiplied = (uint16_t)color * (uint16_t) brightness;
  return multiplied >> 8;
}

void wheel(uint8_t pos, uint8_t* out){
  pos = 255 - pos;
  if(pos < 85) {
    out[0] = 255 - pos * 3;
    out[1] = 0;
    out[2] = pos * 3;
  }else if(pos < 170) {
    pos -= 85;
    out[0] =0;
    out[1] = pos * 3;
    out[2] = 255 - pos * 3;
  }else{
    pos -= 170;
    out[0] = pos * 3;;
    out[1] = 255 - pos * 3;
    out[2] = 0;
  }
}

void WS2812Runner::wake(){
  xTaskNotify( taskHandle,
               0,
               eNoAction);
}


void WS2812Runner::set_effect_arguments(EffectArguments *args){
  if(args != effect_arguments){
    effect_arguments__free_unpacked(effect_arguments, NULL);
  }

  EffectState *new_effect_state = (EffectState *)malloc(sizeof(EffectState));
  EffectState *old_effect_state = effect_state;
  effect_state__init(new_effect_state);
  effect_arguments  = args;

  generate_effect_state(args, new_effect_state);
  effect_state = new_effect_state;
  effect_state__free_unpacked(old_effect_state, NULL);
  wake();
}

int WS2812Runner::executeEffect(){
  ESP_LOGD(TAG, "e");
  if(effect_arguments -> power_state){
    void (*effect)(WS2812_strip *, void*,void*) = select_effect(effect_arguments->effect_arguments_case);
    effect(strip, effect_arguments, effect_state);
    return 20;
  }else{
    strip -> reset_all_leds();
    return -1;
  }
}

WS2812Runner::~WS2812Runner(){
  stop();
}

void WS2812Runner::stop(){
  vTaskDelete(taskHandle);
  strip->reset_all_leds();
}

