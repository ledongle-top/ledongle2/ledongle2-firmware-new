#include "millis.h"
#include "esp_timer.h";
uint32_t millis(void) {
  return esp_timer_get_time() / 1000;
}
