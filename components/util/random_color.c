#include "color.h"
#include <esp_system.h>

uint32_t random_color(){
  uint16_t hue = esp_random();
  return RANDOM_COLOR_BIT & hsv_to_rgb(hue, 255, 255);
}
