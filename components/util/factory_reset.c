#include <esp_partition.h>
#include <esp_ota_ops.h>
#include <esp_system.h>

void factory_reset(void){
  esp_ota_set_boot_partition(esp_partition_find_first(ESP_PARTITION_TYPE_APP, ESP_PARTITION_SUBTYPE_APP_FACTORY, NULL));
  esp_restart();
}
