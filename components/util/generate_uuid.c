#include <esp_system.h>
#include "generate_uuid.h"
#include <stdio.h>
char *generate_uuid(){
  uint8_t uuid[16];
  uint32_t *chunks = (uint32_t *)uuid;
  for(int i = 0; i < 4; i++){
    chunks[i] = esp_random();
  }
  uuid[6] &= 0x0f;
  uuid[6] |= 0x40;
  uuid[7] &= 0x3f;
  uuid[7] |= 0x80;
  char *result = (char*)malloc(37);
  sprintf(result, "%.2hhx%.2hhx%.2hhx%.2hhx-%.2hhx%.2hhx-%.2hhx%.2hhx-%.2hhx%.2hhx-%.2hhx%.2hhx%.2hhx%.2hhx%.2hhx%.2hhx", uuid[0], uuid[1], uuid[2], uuid[3], uuid[4], uuid[5], uuid[6], uuid[7], uuid[8], uuid[9], uuid[10], uuid[11], uuid[12], uuid[13], uuid[14], uuid[15] );
  return result;
}
