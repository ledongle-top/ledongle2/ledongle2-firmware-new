#ifndef COLOR_UTIL_H
#define COLOR_UTIL_H
#ifdef __cplusplus
extern "C" {
#endif

#include "stdint.h"

#define RGB(r,g,b) ((uint32_t)r << 16 | (uint32_t) g << 8 | (uint32_t) b)
#define R(rgb) (uint8_t)((rgb >> 16)&0xff)
#define G(rgb) (uint8_t)((rgb >> 8)&0xff)
#define B(rgb) (uint8_t)((rgb)&0xff)

#define RANDOM_COLOR_BIT (1 << 31)

  uint32_t random_color();


  uint32_t hsv_to_rgb(uint16_t h, uint8_t s, uint8_t v);
  extern const uint8_t dim_curve[256];
#ifdef __cplusplus
}
#endif

#endif
