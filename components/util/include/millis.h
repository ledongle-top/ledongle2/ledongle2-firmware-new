#pragma once
#ifdef __cplusplus
extern "C"{
#endif
#include <stdint.h>

uint32_t millis(void);
#ifdef __cplusplus
}
#endif
