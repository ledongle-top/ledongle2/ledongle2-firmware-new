#include "WS2812Dummy.h"
#include <esp_log.h>
static const char* TAG = "DUMMYSTRIP";

void WS2812Dummy::setLed(uint16_t i, uint8_t r, uint8_t g, uint8_t b){
  if(i < num){
    buffer[i*3] = r;
    buffer[i*3+1] = g;
    buffer[i*3+2] = b;
  }else{
    ESP_LOGE(TAG, "LED index out of range. i: %d, strip length: %d", i, num);
  }
}
void WS2812Dummy::setAllLeds(uint8_t r, uint8_t g, uint8_t b){
  for(size_t i = 0; i < num; i++){
    setLed(i, r,g,b);
  }
}
void WS2812Dummy::showFrame(){
  ESP_LOGD(TAG, "Show frame");
}
void WS2812Dummy::setNum(int num){
  if((buffer = (uint8_t*)realloc(buffer, num*3)) == NULL){
    ESP_LOGE(TAG, "reallocing buffer failed");
    this->num = 0;
  }else{
    this->num = num;
  }
}
void WS2812Dummy::setBrightness(uint8_t brightness){
  this->brightness = brightness;
}
void WS2812Dummy::resetAllLeds(){
  memset(buffer, 0, num* 3);
}

WS2812Dummy::WS2812Dummy(size_t num):WS2812_strip(num){
  buffer = (uint8_t*)malloc(num*3);
  if(buffer == NULL){
    ESP_LOGE(TAG, "Failed to alloc buffer");
    num = 0;
  }
}
WS2812Dummy::~WS2812Dummy(){
  free(buffer);
}
