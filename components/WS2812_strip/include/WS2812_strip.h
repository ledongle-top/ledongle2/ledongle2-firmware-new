#ifndef WS2812_STRIP_H
#define WS2812_STRIP_H
#include <stdint.h>
class WS2812_strip{
private:
  WS2812_strip();


 public:
  uint16_t num;
  virtual void set_all_leds(uint8_t r, uint8_t g, uint8_t b) = 0;
  virtual void showFrame() = 0;
  virtual void reset_all_leds() = 0;
  virtual void set_led(uint16_t i, uint8_t r, uint8_t g, uint8_t b) = 0;
  virtual uint32_t get_led(uint16_t i) = 0;
protected:
  uint8_t brightness;
  void *effect_state;

protected:
  WS2812_strip(uint16_t num):num(num){};

};
#endif
