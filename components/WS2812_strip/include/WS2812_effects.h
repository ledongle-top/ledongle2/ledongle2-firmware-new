#ifndef WS2812_EFFECTS_H
#define WS2812_EFFECTS_H
#ifndef MIN
#define MIN(a,b) (a<b?a:b)
#endif
#define MAX(a,b) (a>b?a:b)
#define MOD(x,m) ((x)>0?(x)>(m)?(x)-(m):(x):(x+m))
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#define VERYSHORTDELAY 10
#define SHORTDELAY 20
#define LONGDELAY 50
#define INFINITEDELAY -1




#endif
