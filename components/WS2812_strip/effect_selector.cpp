
#include "WS2812_strip.h"
#include "effects.h"
#include "strip_config.pb-c.h"
void (*select_effect(EffectArguments__EffectArgumentsCase argType))(WS2812_strip*, EffectArguments*, EffectState*){
  switch(argType){
#include "effect_selector.cblock"
  default:
    return NULL;
  }
}
