#include <unity.h>
#include "WS2812Runner.h"
#include "WS2812Dummy.h"

TEST_CASE("Test WS2812 Runner command handling", "[runner]")
{
  /*  WS2812Runner testRunner = WS2812Runner(new WS2812Dummy(1));
  testRunner.start();
  struct LedCommand cmd = {COMMAND_NONE, 39, -1, -1, -1, -1};
  testRunner.sendCommand(&cmd);
  Future<struct LedCommand> future = Future<struct LedCommand>();
  testRunner.getStatus(&future);
  future.await(&status, 0);
  vTaskDelay(100);
  TEST_ASSERT_EQUAL(39, status.r);*/
}
TEST_CASE("Test LED command formatting and parsing", "[runner]"){
  struct LedCommand cmd = {RAINBOW, 1, 2, 3, 4, 5, NULL};
  cJSON *json = ledCommandToJSON(cmd);
  struct LedCommand parsed = ledCommandFromJSON(json);
  TEST_ASSERT_EQUAL(cmd.cmd, parsed.cmd);
  TEST_ASSERT_EQUAL(cmd.r, parsed.r);
  TEST_ASSERT_EQUAL(cmd.g, parsed.g);
  TEST_ASSERT_EQUAL(cmd.b, parsed.b);
  TEST_ASSERT_EQUAL(cmd.brightness, parsed.brightness);
  TEST_ASSERT_EQUAL(cmd.num, parsed.num);
  cJSON_Delete(json);
}
