#include "WS2812_dled.h"
#include "esp_log.h"
#include <color.h>

static const char* TAG = "WS2812DLED";


void WS2812_dled_strip::reset_all_leds(){
  for(uint16_t i = 0; i < num; i++){
    set_led(i, 0,0,0);
  }
}

void WS2812_dled_strip::set_led(uint16_t i, uint8_t r, uint8_t g, uint8_t b){
  if(i < strip.length){
    uint16_t r_adjusted = ((uint16_t)r * brightness)/100;
    uint16_t g_adjusted = ((uint16_t)g * brightness)/100;
    uint16_t b_adjusted = ((uint16_t)b * brightness)/100;
    dled_pixel_set(strip.pixels+i,  (uint8_t)r_adjusted, (uint8_t)g_adjusted, (uint8_t)b_adjusted);
    full_brightness_buffer[i*3] = r;
    full_brightness_buffer[i*3+1] = g;
    full_brightness_buffer[i*3+2] = b;
  }else{
    ESP_LOGE(TAG, "Tried to set led %d on strip of length %u", i, strip.length);
  }
}

void WS2812_dled_strip::set_all_leds(uint8_t r, uint8_t g, uint8_t b){
  for(uint16_t i = 0; i < num; i++){
    set_led(i, r, g, b);
  }
}

uint32_t WS2812_dled_strip::get_led(uint16_t i){
  if(i < num){
    return RGB(full_brightness_buffer[i*3],
               full_brightness_buffer[i*3+1],
               full_brightness_buffer[i*3+2]);
  }else{
    return 0;
  }
}

void WS2812_dled_strip::showFrame(){
  dled_strip_fill_buffer(&strip);
  rmt_dled_send(&rps);
}

WS2812_dled_strip::WS2812_dled_strip(int gpio, size_t num, uint8_t brightness, int strip_id):WS2812_strip(num), brightness(brightness){
  esp_err_t err;
  dled_strip_init(&strip);
  dled_strip_create(&strip, DLED_WS2812B, num, 255);
  full_brightness_buffer = (uint8_t*)malloc(num*3);
  rmt_dled_init(&rps);

  err = rmt_dled_create(&rps, &strip);
  if (err != ESP_OK) {
    ESP_LOGE(TAG, "[0x%x] rmt_dled_init failed", err);
  }

  err = rmt_dled_config(&rps,(gpio_num_t) gpio, (rmt_channel_t)strip_id);
  if (err != ESP_OK) {
    ESP_LOGE(TAG, "[0x%x] rmt_dled_config failed", err);
  }

}
