#ifndef WS2812_DLED_H
#define WS2812_DLED_H
#include "WS2812_strip.h"
#include "esp32_rmt_dled.h"

class WS2812_dled_strip: public WS2812_strip{
 private:
  rmt_pixel_strip_t rps;
  pixel_strip_t strip;
  uint8_t brightness;
  uint8_t *full_brightness_buffer;
 public:
  void reset_all_leds();
  void set_led(uint16_t i, uint8_t r, uint8_t g, uint8_t b);
  void set_all_leds(uint8_t r, uint8_t g, uint8_t b);
  void showFrame();
  WS2812_dled_strip(int gpio, size_t num, uint8_t brightness, int strip_id);
  uint32_t get_led(uint16_t i);
};
#endif
