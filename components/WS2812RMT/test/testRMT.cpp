#include <unity.h>
#include "WS2812RMT.h"
WS2812RMT_strip *strip;
TEST_CASE("Initialize LED strip on pin 27 with 5 LEDs", "[WS2812RMT]"){
  strip = new WS2812RMT_strip(27, 5);
}
TEST_CASE("Set all LEDs to RED", "[WS2812RMT]"){
  if(strip == NULL){
    printf("Error: Strip not initialized");
  }else{
    strip->setAllLeds(255,0,0);
    strip->showFrame();
  }
}
TEST_CASE("Reset all LEDs", "[WS2812RMT]"){
  if(strip == NULL){
    printf("Error: Strip not initialized");
  }else{
    strip->resetAllLeds();
  }
}

TEST_CASE("Change number of LEDs to 10", "[WS2812RMT]"){
  if(strip == NULL){
    printf("Error: Strip not initialized");
  }else{
    strip->setNum(10);
  }
}
