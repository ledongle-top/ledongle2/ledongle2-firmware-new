var searchData=
[
  ['read',['read',['../classESP32CPP_1_1GPIO.html#add68250ce86665c502405a353ef1c870',1,'ESP32CPP::GPIO::read()'],['../classI2C.html#ac81d33d8192945eb2caa210ae5055851',1,'I2C::read(uint8_t *bytes, size_t length, bool ack=true)'],['../classI2C.html#a732035bfdeb90745e21e3c0cc6a6992c',1,'I2C::read(uint8_t *byte, bool ack=true)'],['../classPCF8574.html#a90a8a8c48e0530c7371dacabc1f6a282',1,'PCF8574::read()'],['../classPCF8575.html#ad8f7990aa6a3bc18e65b21818ca9f711',1,'PCF8575::read()']]],
  ['readaccel',['readAccel',['../classMPU6050.html#ad4e58ad436a52f96e7172e7081fe9afb',1,'MPU6050']]],
  ['readbit',['readBit',['../classPCF8574.html#a4e484981cdf6451bb36a13f9d3a60400',1,'PCF8574::readBit()'],['../classPCF8575.html#a9d6da65bfa0a804b677fb2f728347380',1,'PCF8575::readBit()']]],
  ['readgyro',['readGyro',['../classMPU6050.html#a2514ae2158f5b87c8267242077ddd0ec',1,'MPU6050']]],
  ['receive',['receive',['../classRingbuffer.html#a2de1d3d2c869875883b351e66212dfef',1,'Ringbuffer::receive()'],['../classSocket.html#abeb5751e47f3fa96755aba55d29c300a',1,'Socket::receive()']]],
  ['receivedata',['receiveData',['../classSockServ.html#a9e4e21eb4e94912fd403a7e3dcbdb54c',1,'SockServ']]],
  ['receivefrom',['receiveFrom',['../classSocket.html#afc27706410db610051b84cb3c969daa3',1,'Socket']]],
  ['remove',['remove',['../classFileSystem.html#a9dbe34309a1663db28419a65cf8850fc',1,'FileSystem']]],
  ['reset',['reset',['../classFreeRTOSTimer.html#ae1f243c23d6d6edece568fe0e9de0209',1,'FreeRTOSTimer::reset()'],['../classOV7670.html#a7148b44da5949ae39468c8c31cf6092f',1,'OV7670::reset()']]],
  ['restart',['restart',['../classSystem.html#a41237d182fff760ea31f960b8d3bf343',1,'System']]],
  ['returnitem',['returnItem',['../classRingbuffer.html#a145b135c71d2e715f13bdbe186e45cc6',1,'Ringbuffer']]],
  ['ringbuffer',['Ringbuffer',['../classRingbuffer.html#a32e23dca7a30bc6d1d4b5c10ecbfbc52',1,'Ringbuffer']]],
  ['rmt',['RMT',['../classRMT.html#ab4e0f3b8ff01d7604d0bc52bc44c590f',1,'RMT']]],
  ['run',['run',['../classTask.html#a399f60ad8cd91d34dba2de57b4ac6d65',1,'Task']]],
  ['rxstart',['rxStart',['../classRMT.html#af1b130bf271e12117511f75f6e08fcea',1,'RMT']]],
  ['rxstop',['rxStop',['../classRMT.html#a0f3abda595084f39bc5a524b468feeab',1,'RMT']]]
];
