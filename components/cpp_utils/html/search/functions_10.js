var searchData=
[
  ['scan',['scan',['../classI2C.html#ad3ebfa3204880e4fa60beeba997f357a',1,'I2C::scan()'],['../classWiFi.html#a97592a3ac47f6f301a2417385c4e8ea2',1,'WiFi::scan()']]],
  ['send',['send',['../classRingbuffer.html#ac4700ffc589022ed2dee5c069f583d7c',1,'Ringbuffer::send()'],['../classSocket.html#ad9757badfe9d9903d07ef7c70279ad77',1,'Socket::send(std::string value) const'],['../classSocket.html#a6d6e3f6b1d0f355575a1bfb350b6477d',1,'Socket::send(const uint8_t *data, size_t length) const'],['../classWebSocket.html#a9632cea9451d149a1dda489a3d0975cf',1,'WebSocket::send(std::string data, uint8_t sendType=SEND_TYPE_BINARY)'],['../classWebSocket.html#a8cb4296ffa57b80cbec6694c4611bdea',1,'WebSocket::send(uint8_t *data, uint16_t length, uint8_t sendType=SEND_TYPE_BINARY)']]],
  ['sendack',['sendAck',['../classTFTP_1_1TFTP__Transaction.html#a46521d26eb68aac975fbb58e706804e8',1,'TFTP::TFTP_Transaction']]],
  ['senddata',['sendData',['../classHttpResponse.html#a68bc032812a96326d9fe4fde668a372b',1,'HttpResponse::sendData()'],['../classSockServ.html#a3e4e4cc30becc9440573c3285e8004d2',1,'SockServ::sendData(uint8_t *data, size_t length)'],['../classSockServ.html#ac50c88830b1b4a3e75b8fbd82799b08d',1,'SockServ::sendData(std::string str)']]],
  ['senderror',['sendError',['../classTFTP_1_1TFTP__Transaction.html#a503c474a79b6b88c36b108bba9236e01',1,'TFTP::TFTP_Transaction']]],
  ['sendto',['sendTo',['../classSocket.html#a44a41324fbee63de97bdadc7de03bda3',1,'Socket']]],
  ['set',['set',['../classNVS.html#aada586fb7a5ae6781fc7d2e2babf9894',1,'NVS']]],
  ['setaddress',['setAddress',['../classI2C.html#ab6ea7b33432cfea858484411741b3bca',1,'I2C']]],
  ['setarray',['setArray',['../classJsonObject.html#a19f385d7149976a9726e84e2ff287317',1,'JsonObject']]],
  ['setbasedir',['setBaseDir',['../classTFTP.html#a13a7562a16fea3acabafc3c0811cabef',1,'TFTP::setBaseDir()'],['../classTFTP_1_1TFTP__Transaction.html#ab1dc0909b4d9416f2148a0ba68453a76',1,'TFTP::TFTP_Transaction::setBaseDir()']]],
  ['setboolean',['setBoolean',['../classJsonObject.html#a325a19e3eaff0b86f36af75f18786bc9',1,'JsonObject']]],
  ['setbrightness',['setBrightness',['../classSmartLED.html#afd7bcb3a6606ee885978c94cc5bd3833',1,'SmartLED']]],
  ['setcallback',['setCallback',['../classPubSubClient.html#a115733ca6dd337362375d3c4a45300fe',1,'PubSubClient']]],
  ['setcallbacks',['setCallbacks',['../classFTPServer.html#a193be61985bf88aa0cc70834a801916a',1,'FTPServer']]],
  ['setchar',['setChar',['../classMAX7219.html#a8a4624d45533a9de52585202f7b2fc78',1,'MAX7219']]],
  ['setclient',['setClient',['../classPubSubClient.html#a30e35b9461d26f3d10736ac39c9de418',1,'PubSubClient']]],
  ['setclienttimeout',['setClientTimeout',['../classHttpServer.html#ad70784b8f9484a16bf74c34413a24d4d',1,'HttpServer']]],
  ['setcolororder',['setColorOrder',['../classSmartLED.html#a1d7155c63ed77ae070575dcbc960292e',1,'SmartLED::setColorOrder()'],['../classWS2812.html#a7a68e53534f4b1f68e447621caf0e2e6',1,'WS2812::setColorOrder()']]],
  ['setcolumn',['setColumn',['../classMAX7219.html#a4cc8fe7ad21942e059ac4a931fce502e',1,'MAX7219']]],
  ['setcore',['setCore',['../classTask.html#a5ec932fb35fd295c78eafcf06af1d91d',1,'Task']]],
  ['setdebug',['setDebug',['../classI2C.html#a846d50b0bd6b866498f91bfc50b565cc',1,'I2C']]],
  ['setdigit',['setDigit',['../classMAX7219.html#aa4f94d965ecde62d2dd8a4091005b0a0',1,'MAX7219']]],
  ['setdirectorylisting',['setDirectoryListing',['../classHttpServer.html#aab1f060cbbaaeaa2b75f812e866e6ff0',1,'HttpServer']]],
  ['setdnsserver',['setDNSServer',['../classWiFi.html#a99cd00c2301a303e7c856192d2e16160',1,'WiFi']]],
  ['setdouble',['setDouble',['../classJsonObject.html#a845838c1d36f3ff586b325b5b56b65bc',1,'JsonObject']]],
  ['setduty',['setDuty',['../classPWM.html#adae9be9a8c34c42d0f81dd0b4a24c14f',1,'PWM']]],
  ['setdutypercentage',['setDutyPercentage',['../classPWM.html#ab2a9dd4cbe6349d420eef610e8909011',1,'PWM']]],
  ['setfilebuffersize',['setFileBufferSize',['../classHttpServer.html#a7686343c224f5e8a62f5f07de481345d',1,'HttpServer']]],
  ['setfrequency',['setFrequency',['../classPWM.html#a51543c5bf29d9f3ae83c7f5af176895d',1,'PWM']]],
  ['sethandler',['setHandler',['../classWebSocket.html#aad812ebe8eb8405006c0ed8c8e567239',1,'WebSocket']]],
  ['sethost',['setHost',['../classSPI.html#a85ec652a3a1eb453f6b9938a45b599c9',1,'SPI']]],
  ['sethsbpixel',['setHSBPixel',['../classSmartLED.html#a0d25c9253f280f79c3fee565d0bd3efd',1,'SmartLED::setHSBPixel()'],['../classWS2812.html#ab351cea856076c96121d5c3b64f2a7f3',1,'WS2812::setHSBPixel()']]],
  ['setinput',['setInput',['../classESP32CPP_1_1GPIO.html#afd43e7bc27885215900f7780197a0247',1,'ESP32CPP::GPIO']]],
  ['setint',['setInt',['../classJsonObject.html#a3e874f6366c532732adbb7dcc50a641c',1,'JsonObject']]],
  ['setintensity',['setIntensity',['../classMAX7219.html#a204133a6b253f5d519acd5dc0077f32a',1,'MAX7219']]],
  ['setinterrupttype',['setInterruptType',['../classESP32CPP_1_1GPIO.html#af649387f8f64c1abdc66d3b93c2e966b',1,'ESP32CPP::GPIO']]],
  ['setinvert',['setInvert',['../classPCF8574.html#a9663079a19f252a5edc2471f384bc28b',1,'PCF8574::setInvert()'],['../classPCF8575.html#a5841b1d96a69b53be8a9a7634e445212',1,'PCF8575::setInvert()']]],
  ['setipinfo',['setIPInfo',['../classWiFi.html#a9af2e6f3f5bf0dcf9a319954cf6ec5a0',1,'WiFi::setIPInfo(const std::string &amp;ip, const std::string &amp;gw, const std::string &amp;netmask)'],['../classWiFi.html#ac791bd68948ead18bc9775fc936e0938',1,'WiFi::setIPInfo(uint32_t ip, uint32_t gw, uint32_t netmask)']]],
  ['setled',['setLed',['../classMAX7219.html#aebc8bfad65290b1b4580688c719f7e13',1,'MAX7219']]],
  ['setmaxfiles',['setMaxFiles',['../classFATFS__VFS.html#ae88702f8fce66d77974571adfe7db5a3',1,'FATFS_VFS']]],
  ['setname',['setName',['../classFreeRTOS_1_1Semaphore.html#a24047020ab820d4f641bc39bfab643df',1,'FreeRTOS::Semaphore::setName()'],['../classTask.html#a23abd79183a5c2bb1e6d3ff37b1cbf23',1,'Task::setName()']]],
  ['setnext',['setNext',['../classDMABuffer.html#abae5632ed93fdb8c200e968640d83b71',1,'DMABuffer']]],
  ['setnexthandler',['setNextHandler',['../classWiFiEventHandler.html#afee45469141ee1351e06609ad0050133',1,'WiFiEventHandler']]],
  ['setnumber',['setNumber',['../classMAX7219.html#a1e50065060426b3433ec16ea2e304760',1,'MAX7219']]],
  ['setobject',['setObject',['../classJsonObject.html#acdf7762d31f04063b1b6a871ae91af0e',1,'JsonObject']]],
  ['setoutput',['setOutput',['../classESP32CPP_1_1GPIO.html#ac1599d926502a487e0e7518dc343a833',1,'ESP32CPP::GPIO']]],
  ['setpixel',['setPixel',['../classSmartLED.html#aa0e7fcf8f943748d68c23dbef90b52a6',1,'SmartLED::setPixel(uint16_t index, uint8_t red, uint8_t green, uint8_t blue)'],['../classSmartLED.html#aee14c9e5278ba735cd9890ab33bb4802',1,'SmartLED::setPixel(uint16_t index, pixel_t pixel)'],['../classSmartLED.html#aae855663a57e10790c6dc892bf78a37d',1,'SmartLED::setPixel(uint16_t index, uint32_t pixel)'],['../classWS2812.html#a60025d23e7fc21c3ce8c1f02bd50bf86',1,'WS2812::setPixel(uint16_t index, uint8_t red, uint8_t green, uint8_t blue)'],['../classWS2812.html#a2f37824dbf4730986d4ebde58c25a48d',1,'WS2812::setPixel(uint16_t index, pixel_t pixel)'],['../classWS2812.html#a65924d977bab621786b01a09c91456f6',1,'WS2812::setPixel(uint16_t index, uint32_t pixel)']]],
  ['setport',['setPort',['../classFTPServer.html#adee541fc29714c9ff5f478892b0efe60',1,'FTPServer::setPort()'],['../classSockServ.html#a12c8f4baa1bad7d405a39c4ad3310e37',1,'SockServ::setPort()']]],
  ['setpriority',['setPriority',['../classTask.html#aaa220be4e5007ede0bd3e5349d36eae9',1,'Task']]],
  ['setreuseaddress',['setReuseAddress',['../classSocket.html#ad435a8a36e2759203db8b4b2c67e4e85',1,'Socket']]],
  ['setrootpath',['setRootPath',['../classHttpServer.html#a141a7cde0ccf11ff395df0faef79d6a5',1,'HttpServer']]],
  ['setrow',['setRow',['../classMAX7219.html#aecd0b20a63757fd74e849369dd797e74',1,'MAX7219']]],
  ['setscanlimit',['setScanLimit',['../classMAX7219.html#a14cd47938ff60879b676d9a0c02d4268',1,'MAX7219']]],
  ['setserver',['setServer',['../classPubSubClient.html#aa809b115adf6c551dee72d12ecfd8414',1,'PubSubClient']]],
  ['setsocketoption',['setSocketOption',['../classSocket.html#a0bee77d0779e5d3a0f62ef7d37e5033d',1,'Socket']]],
  ['setssl',['setSSL',['../classSocket.html#a73d0af1a733ff70a622eac54a997f887',1,'Socket']]],
  ['setstacksize',['setStackSize',['../classTask.html#afe0f5594c56b8cc7ac96c2198d5d81b4',1,'Task']]],
  ['setstatus',['setStatus',['../classHttpResponse.html#af13a61ae987f15599232fa30d2cfdeab',1,'HttpResponse']]],
  ['setstring',['setString',['../classJsonObject.html#ac080c7b7037401cb2b0034c7f6220026',1,'JsonObject']]],
  ['settimeout',['setTimeout',['../classSocket.html#aef6249eaa9b2be5a386f85cb828c2b4a',1,'Socket']]],
  ['setwifieventhandler',['setWifiEventHandler',['../classWiFi.html#abf6df73f23fed380a5109a29def8d08f',1,'WiFi']]],
  ['show',['show',['../classApa102.html#af565f955f47a19ffb645a21ebbc5bc06',1,'Apa102::show()'],['../classWS2812.html#ad03797836617dccf530cb7c6423c03fe',1,'WS2812::show()']]],
  ['shutdown',['shutdown',['../classMAX7219.html#a948326a74baa10f8fead88c7b04a21b1',1,'MAX7219']]],
  ['size',['size',['../classJsonArray.html#a3fdfa5f61149d0e8ca1f443762b54618',1,'JsonArray']]],
  ['slavepresent',['slavePresent',['../classI2C.html#a4acd5dd262eb4638e81f471a5e605194',1,'I2C']]],
  ['sleep',['sleep',['../classFreeRTOS.html#abea19fd141147c054f47c6a00e77f2ff',1,'FreeRTOS']]],
  ['socketinputrecordstreambuf',['SocketInputRecordStreambuf',['../classSocketInputRecordStreambuf.html#a0ed684a3ce6c98cd806c35fd551937c3',1,'SocketInputRecordStreambuf']]],
  ['sockserv',['SockServ',['../classSockServ.html#a8c3fa6bf7bb34ebf9b8db86048ded61c',1,'SockServ::SockServ(uint16_t port)'],['../classSockServ.html#a9fb32006f0a3a89da0145971a17ccab4',1,'SockServ::SockServ()']]],
  ['spi',['SPI',['../classSPI.html#a2ba081c29fbdecc704c6bf00b24d5205',1,'SPI']]],
  ['split',['split',['../classGeneralUtils.html#addd9bb2f367a86841e48f5aa67f1d003',1,'GeneralUtils']]],
  ['staauthchange',['staAuthChange',['../classWiFiEventHandler.html#a333ac6008f9823ef3053883d272fb0ba',1,'WiFiEventHandler']]],
  ['staconnected',['staConnected',['../classNeoPixelWiFiEventHandler.html#a3f0c1253e8355f4abe1d8dee09db1fec',1,'NeoPixelWiFiEventHandler::staConnected()'],['../classWiFiEventHandler.html#a1f2cc8efb4002ec195b1d0bba50c1cb2',1,'WiFiEventHandler::staConnected()']]],
  ['stadisconnected',['staDisconnected',['../classNeoPixelWiFiEventHandler.html#a3cc5b4d8d4bd3385972d1a1ae864f46d',1,'NeoPixelWiFiEventHandler::staDisconnected()'],['../classWiFiEventHandler.html#a2a9e3223a299eea32d1654283151d8d4',1,'WiFiEventHandler::staDisconnected()']]],
  ['stagotip',['staGotIp',['../classNeoPixelWiFiEventHandler.html#a2c1336ec495c8aa04fd5118932a479bb',1,'NeoPixelWiFiEventHandler::staGotIp()'],['../classWiFiEventHandler.html#ae3081968ca143a3694dfa00ac2006adf',1,'WiFiEventHandler::staGotIp()']]],
  ['start',['start',['../classFreeRTOSTimer.html#a4cad6643b084ed3813a424fa60547742',1,'FreeRTOSTimer::start()'],['../classFTPServer.html#a84264700de3f7a384a885977145ba9c0',1,'FTPServer::start()'],['../classHttpServer.html#a79bbf11522346b2eae709e6112b130c2',1,'HttpServer::start()'],['../classI2C.html#a07463a2babdabbd675a2745fb53650ca',1,'I2C::start()'],['../classSockServ.html#abbfb5d8eba2ef0a47fa0aa981a43f6dc',1,'SockServ::start()'],['../classTask.html#a6f13d13787e21dc929b61d112b863433',1,'Task::start()'],['../classTFTP.html#a8a8386ee01e4bad105cb1fee4e7aae51',1,'TFTP::start()']]],
  ['startap',['startAP',['../classWiFi.html#ae5d8dc106189ee9a4cf332e4363fac58',1,'WiFi::startAP(const std::string &amp;ssid, const std::string &amp;passwd, wifi_auth_mode_t auth=WIFI_AUTH_OPEN)'],['../classWiFi.html#afe587b246a0a1a4024877bebd1dd3453',1,'WiFi::startAP(const std::string &amp;ssid, const std::string &amp;passwd, wifi_auth_mode_t auth, uint8_t channel, bool ssid_hidden, uint8_t max_connection)']]],
  ['starttask',['startTask',['../classFreeRTOS.html#aaabf67574ec5a28c3db4536e200af576',1,'FreeRTOS']]],
  ['stascandone',['staScanDone',['../classWiFiEventHandler.html#a1f7d236b42b7dd636253f92a2f417649',1,'WiFiEventHandler']]],
  ['state',['state',['../classPubSubClient.html#a8a7025562e55ee15fd4469b75d535b1d',1,'PubSubClient']]],
  ['stop',['stop',['../classFreeRTOSTimer.html#adfa6641c22dba498dcd44744294d072d',1,'FreeRTOSTimer::stop()'],['../classHttpServer.html#aad9c63d3f8cda248e3c87067ac3ac5a5',1,'HttpServer::stop()'],['../classI2C.html#ac9a9e5ea511ff86f474866052a7fabf1',1,'I2C::stop()'],['../classPWM.html#a839027de87bbee57a3dabe21d254d892',1,'PWM::stop()'],['../classSockServ.html#a4d6d90089cd108ba10e481e4aca274f4',1,'SockServ::stop()'],['../classTask.html#aba5eb3d6c2a034aa0e319383fbec68c4',1,'Task::stop()']]],
  ['subscribe',['subscribe',['../classPubSubClient.html#a62ae269568ef379fb21a434f7e6d4a06',1,'PubSubClient']]]
];
