var searchData=
[
  ['default_5fclk_5fpin',['DEFAULT_CLK_PIN',['../classI2C.html#aece11572688a31925150176f2241e956',1,'I2C::DEFAULT_CLK_PIN()'],['../classSPI.html#a26f31b5eb7b69e66761d387116ea0003',1,'SPI::DEFAULT_CLK_PIN()']]],
  ['default_5fclk_5fspeed',['DEFAULT_CLK_SPEED',['../classI2C.html#a3343eadb26bb775ed15e7b57ae9abd88',1,'I2C']]],
  ['default_5fcs_5fpin',['DEFAULT_CS_PIN',['../classSPI.html#a7ed47313c92be5762bb61a6d69a58d7e',1,'SPI']]],
  ['default_5fmiso_5fpin',['DEFAULT_MISO_PIN',['../classSPI.html#af64304a498004d8abd46931cfbb5bba6',1,'SPI']]],
  ['default_5fmosi_5fpin',['DEFAULT_MOSI_PIN',['../classSPI.html#a311aea82b4ba1b8b57ac88ec62c94bf2',1,'SPI']]],
  ['default_5fsda_5fpin',['DEFAULT_SDA_PIN',['../classI2C.html#a4d371d14b0fcae3b20f22ee8254536b8',1,'I2C']]]
];
