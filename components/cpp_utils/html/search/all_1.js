var searchData=
[
  ['base64decode',['base64Decode',['../classGeneralUtils.html#af43c8ecc3449ea441a2a8e88a50a34d6',1,'GeneralUtils']]],
  ['base64encode',['base64Encode',['../classGeneralUtils.html#abe8373207df0ef16bc66338b4ec8d298',1,'GeneralUtils']]],
  ['begintransaction',['beginTransaction',['../classI2C.html#a414fa12d9ddd715be6802cd6fbe8d0ed',1,'I2C']]],
  ['bind',['bind',['../classSocket.html#adc1e8efb9384ac637d42e93008debb6a',1,'Socket']]],
  ['blebeacon',['BLEBeacon',['../classBLEBeacon.html',1,'']]],
  ['bledisconnectedexception',['BLEDisconnectedException',['../classBLEDisconnectedException.html',1,'']]],
  ['bleeddystonetlm',['BLEEddystoneTLM',['../classBLEEddystoneTLM.html',1,'']]],
  ['bleeddystoneurl',['BLEEddystoneURL',['../classBLEEddystoneURL.html',1,'']]],
  ['bleuuidnotfoundexception',['BLEUuidNotFoundException',['../classBLEUuidNotFoundException.html',1,'']]],
  ['blue',['blue',['../structpixel__t.html#a32f72c7c5c7c932c7e554d05f7390941',1,'pixel_t']]]
];
