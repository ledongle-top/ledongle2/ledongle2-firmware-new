var searchData=
[
  ['endswith',['endsWith',['../classGeneralUtils.html#ade237631b5595e2426f9e86fc6e38932',1,'GeneralUtils']]],
  ['endtransaction',['endTransaction',['../classI2C.html#ad43e562aaf70eac7acca8fde9f904234',1,'I2C']]],
  ['erase',['erase',['../classNVS.html#aa0a90a5ea01bccb557c3a18d701ef859',1,'NVS::erase()'],['../classNVS.html#a3c9194213a36196cc2915d2b7b1a2bae',1,'NVS::erase(std::string key)']]],
  ['errortostring',['errorToString',['../classGeneralUtils.html#a25e466e54ff002f54bfb7e1f43bd401b',1,'GeneralUtils']]],
  ['esp32_20c_2b_2b_20utility_20classes',['ESP32 C++ Utility Classes',['../index.html',1,'']]]
];
