#include "ledongle_wifi.h"
#include <esp_wifi.h>
#include <esp_log.h>
#include <freertos/event_groups.h>
#include <freertos/timers.h>
#include "esp_event_loop.h"
#include <system_config.h>

#define check_return_code(err) if(err != ESP_OK){       \
    systemStatus -> wifi_state = WIFI_STATE__FAILED;    \
    update_system_status(systemStatus);                 \
    return err;}


static const char* TAG = "WIFI";
static TimerHandle_t connection_timeout_timer;

//TODO update system state
static esp_err_t event_handler(void *ctx, system_event_t *event)
{
  switch (event->event_id) {
  case SYSTEM_EVENT_STA_START:
    esp_wifi_connect();
    break;
  case SYSTEM_EVENT_STA_GOT_IP:
    ESP_LOGI(TAG, "wifi connected");
    systemStatus -> wifi_state = WIFI_STATE__CONNECTED;
    update_system_status(systemStatus);
    break;
  case SYSTEM_EVENT_STA_DISCONNECTED:
    ESP_LOGI(TAG, "wifi disconnected");
    if(systemStatus -> wifi_state == WIFI_STATE__CONNECTING){
      systemStatus -> wifi_state = WIFI_STATE__FAILED;
    }else{
      systemStatus -> wifi_state = WIFI_STATE__DISCONNECTED;
    }
    update_system_status(systemStatus);
    break;
  default:
    break;
  }
  return ESP_OK;
}

static void connection_timeout_handler( TimerHandle_t xTimer ){
  if(systemStatus -> wifi_state == WIFI_STATE__CONNECTING){
    wifi_disconnect();
    systemStatus -> wifi_state = WIFI_STATE__FAILED;
    update_system_status(systemStatus);
  }
}


esp_err_t wifi_init(){
  connection_timeout_timer = xTimerCreate(NULL, pdMS_TO_TICKS(systemConfig->wifi->connection_timeout), pdFALSE,NULL, connection_timeout_handler);


  tcpip_adapter_init();
  check_return_code(esp_event_loop_init(event_handler, NULL) );
  wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
  check_return_code(esp_wifi_init(&cfg) );
  check_return_code(esp_wifi_set_storage(WIFI_STORAGE_RAM) );
  check_return_code(esp_wifi_set_mode(WIFI_MODE_STA) );
  check_return_code(esp_wifi_start());

  /* In order to simplify example, channel is set after WiFi started.
   * This is not necessary in real application if the two ices have
   * been already on the same channel.
   */
  check_return_code(esp_wifi_set_channel(13, WIFI_SECOND_CHAN_NONE) );
  check_return_code(esp_wifi_set_max_tx_power(82));
  check_return_code(esp_wifi_set_protocol(ESP_IF_WIFI_STA, WIFI_PROTOCOL_11B|WIFI_PROTOCOL_11G|WIFI_PROTOCOL_11N) );
  systemStatus -> wifi_state = WIFI_STATE__DISCONNECTED;
  update_system_status(systemStatus);
  return ESP_OK;
}

esp_err_t wifi_connect(){
  ESP_LOGI(TAG, "connecting to wifi");
  systemStatus -> wifi_state = WIFI_STATE__CONNECTING;
  update_system_status(systemStatus);
  char *config_ssid = systemConfig -> wifi -> ssid;
  char *config_password = systemConfig -> wifi -> password;
  wifi_config_t wifi_config = {};
  strncpy((char*)wifi_config.sta.ssid, config_ssid, 32);
  strncpy((char*)wifi_config.sta.password, config_password, 64);
  ESP_LOGI(TAG, "Setting WiFi configuration SSID %s", wifi_config.sta.ssid);
  check_return_code(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config)) ;
  check_return_code(esp_wifi_connect());
  return ESP_OK;
}

esp_err_t wifi_disconnect(){
  esp_wifi_disconnect();
  check_return_code(esp_wifi_set_channel(13, WIFI_SECOND_CHAN_NONE) );
  return ESP_OK;
}
