#pragma once
#include <esp_err.h>

esp_err_t wifi_init();
esp_err_t wifi_connect();
esp_err_t wifi_disconnect();
