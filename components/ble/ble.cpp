#include "ble.h"
#include <system_config.h>
#include <system_commands.h>
#include <BLEServer.h>
#include <BLEDevice.h>
#include <esp_log.h>
#include <stdint.h>
#include <protos.h>
#include <proto_manager.h>
#include <esp_ota_ops.h>
#include <ota.h>

static const char* SYSTEM_SERVICE_UUID = "5123c0e3-2134-44a9-a2d0-af205586bfdd";
static const char* SYSTEM_CONFIG_CHARACTERISTIC_UUID = "1a580969-2eaa-44e3-8643-ea0f6c8d5f97";
static const char* SYSTEM_COMMAND_CHARACTERISTIC_UUID = "4e8ca57b-3218-48aa-b1b9-175280e32b6d";
static const char* EFFECT_INFO_CHARACTERISTIC_UUID = "35bb00f6-67a9-47ad-8408-2c3c10f77e43";
static const char* SYSTEM_STATUS_CHARACTERISTIC_UUID = "db67de50-3e9a-46bb-9e7e-df3edf9a5eb0";
static const char* STRIP_SERVICE_UUID = "dcbe3082-3cef-481e-8138-362b1c1aa555";

static const char TAG[] = "BLE";

static BLECharacteristic *systemStatusCharacteristic;
static BLECharacteristic **effectArgumentsCharacteristics;

class SystemStatusBLECallbacks: public BLECharacteristicCallbacks{
  void onRead(BLECharacteristic *characteristic){
    size_t required_size = system_status__get_packed_size(systemStatus);
    uint8_t *buffer = (uint8_t*)malloc(required_size);
    system_status__pack(systemStatus, buffer);
    characteristic -> setValue(buffer, required_size);
    free(buffer);
  }
};

class SystemConfigBLECallbacks: public BLECharacteristicCallbacks{

  void onWrite(BLECharacteristic *characteristic){
    ESP_LOGI(TAG, "rx %d bytes",characteristic -> getValue().length());
    SystemConfig *config = system_config__unpack(NULL, characteristic -> getValue().length(), characteristic -> getData());
    if(config == NULL){
      ESP_LOGE(TAG, "Failed to unpack sys config");
      return;
    }
    set_system_config(config);
  }

  void onRead(BLECharacteristic *characteristic){
    size_t required_size = system_config__get_packed_size(systemConfig);
    uint8_t *buffer = (uint8_t*)malloc(required_size);
    system_config__pack(systemConfig, buffer);
    characteristic -> setValue(buffer, required_size);
    free(buffer);
  }
};

class SystemCommandBLECallbacks: public BLECharacteristicCallbacks{
  void onWrite(BLECharacteristic *characteristic){
    SystemCommand *command = system_command__unpack(NULL, characteristic -> getValue().length(), characteristic -> getData());
    if(command == NULL){
      ESP_LOGW(TAG, "Failed to unpack SystemCommand");
      return;
    }
    execute_system_command(command);
  }
};



class EffectInfoBLECallbacks: public BLECharacteristicCallbacks{
  int proto_length = strlen(protos);
  char buffer[501];
  void onWrite(BLECharacteristic *characteristic){
    size_t proto_length = strlen(protos);
    ESP_LOGI(TAG, "rx %d bytes",characteristic -> getValue().length());
    if(characteristic -> getValue().length() != 4){
      ESP_LOGW(TAG, "Did not receive four bytes on effect info characteristic.");
      characteristic -> setValue("");
      return;
    }

    size_t index = (characteristic->getValue().at(0) << 24) | (characteristic->getValue().at(1) << 16)| (characteristic->getValue().at(2) << 8) | (characteristic->getValue().at(3));
    if(index >= proto_length){
      ESP_LOGW(TAG, "Index out of range");
      characteristic -> setValue("");
      return;
    }
    strncpy(buffer, protos+index, 500);
    buffer[500] = 0;
    characteristic -> setValue(buffer);
  }
};


class EffectArgumentsBLECallbacks: public BLECharacteristicCallbacks{
  uint8_t strip_id;
  void onWrite(BLECharacteristic *characteristic){
    ESP_LOGI(TAG, "rx %d bytes",characteristic -> getValue().length());
    EffectArguments *args = effect_arguments__unpack(NULL, characteristic -> getValue().length(), characteristic -> getData());
    if(args == NULL){
      ESP_LOGE(TAG, "Failed to unpack effect args");
      return;
    }
    set_strip_args(strip_id, args, false);
  }

  void onRead(BLECharacteristic *characteristic){
    EffectArguments *args = strip_runners[strip_id]->get_effect_arguments();
    size_t required_size = effect_arguments__get_packed_size(args);
    uint8_t *buffer = (uint8_t*)malloc(required_size);
    effect_arguments__pack(args, buffer);
    characteristic -> setValue(buffer, required_size);
    free(buffer);
  }
public: EffectArgumentsBLECallbacks(uint8_t strip_id): strip_id(strip_id){}
};

static void update_notification_watcher(void *args){
  for(;;){
    EventBits_t events = wait_for_status_update(SYSTEM_STATUS_UPDATE | EFFECT_ARGUMENTS_UPDATE);
    if(events & SYSTEM_STATUS_UPDATE){
      systemStatusCharacteristic -> notify();
      ESP_LOGI(TAG, "Notify sys status change");
    }
    if(events & EFFECT_ARGUMENTS_UPDATE){
      for(int i = 0; i < systemConfig -> n_strip_config; i++){
        effectArgumentsCharacteristics[i] -> notify();
        ESP_LOGI(TAG, "Notify effect arg change");
      }
    }
  }
}

esp_err_t ble_init(void){
  BLEDevice::init(systemConfig -> device_name);
  BLEServer *bleServer = BLEDevice::createServer();

  BLEService *systemService = bleServer->createService(SYSTEM_SERVICE_UUID);

  BLECharacteristic *systemConfigCharacteristic = systemService -> createCharacteristic(SYSTEM_CONFIG_CHARACTERISTIC_UUID, BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_INDICATE | BLECharacteristic::PROPERTY_WRITE);
  systemConfigCharacteristic -> setCallbacks(new SystemConfigBLECallbacks);

  BLECharacteristic *systemCommandCharacteristic = systemService -> createCharacteristic(SYSTEM_COMMAND_CHARACTERISTIC_UUID, BLECharacteristic::PROPERTY_WRITE);
  systemCommandCharacteristic -> setCallbacks(new SystemCommandBLECallbacks);

  systemStatusCharacteristic = systemService -> createCharacteristic(SYSTEM_STATUS_CHARACTERISTIC_UUID, BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_INDICATE);
  systemStatusCharacteristic -> setCallbacks(new SystemStatusBLECallbacks);

  BLECharacteristic *effectInfoCharacteristic = systemService -> createCharacteristic(EFFECT_INFO_CHARACTERISTIC_UUID, BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_WRITE);
  effectInfoCharacteristic -> setCallbacks(new EffectInfoBLECallbacks);

  systemService->start();

  BLEService *stripService = bleServer->createService(STRIP_SERVICE_UUID);
  effectArgumentsCharacteristics = (BLECharacteristic**) malloc(sizeof(BLECharacteristic*) * systemConfig -> n_strip_config);
  for(uint8_t i = 0; i < systemConfig -> n_strip_config; i++){
    effectArgumentsCharacteristics[i] = stripService -> createCharacteristic(systemConfig -> strip_config[i] -> id, BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_INDICATE | BLECharacteristic::PROPERTY_WRITE);
    effectArgumentsCharacteristics[i] -> setCallbacks(new EffectArgumentsBLECallbacks(i));
  }

  stripService -> start();

  BLEAdvertising *advertiser = bleServer->getAdvertising();
  BLEAdvertisementData adData;
  adData.setManufacturerData(std::string(esp_ota_get_app_description() -> version, VERSION_STRING_LENGTH));
  adData.setPartialServices(BLEUUID(SYSTEM_SERVICE_UUID));
  advertiser -> setAdvertisementData(adData);
  advertiser->start();

  xTaskCreate(update_notification_watcher, "BLENotify", 4096, NULL, 10, NULL);

  return ESP_OK;
}
