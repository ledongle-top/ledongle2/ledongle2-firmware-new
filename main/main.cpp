/* Hello World Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_spi_flash.h"
#include "ble.h"
#include "nvs_flash.h"
#include "system_config.h"
#include <esp_log.h>
#include "strip_config.pb-c.h"
#include <string>
#include <blinksync.h>
#include "factory_reset.h"
#include <ledongle_wifi.h>
#include <ota.h>
#include <driver/adc.h>
#include "esp_ota_ops.h"


#define HALL_THRESHOLD_LOW 0
#define HALL_THRESHOLD_HIGH 200

static const char* TAG = "MAIN";
extern "C"{
    void app_main();
}

void test_task(void *nothing){
    for(;;){
        ESP_LOGD(TAG, "free heap: %u", heap_caps_get_free_size(MALLOC_CAP_8BIT));

        vTaskDelay(10);
    }
}



bool check_factory_reset(){
    adc1_config_width(ADC_WIDTH_BIT_12);
    int hall = hall_sensor_read();
    bool hall_low = hall < HALL_THRESHOLD_LOW;
    bool hall_high = hall > HALL_THRESHOLD_HIGH;
    if( hall_low || hall_high ){
        vTaskDelay(pdMS_TO_TICKS(10000));
        int hall = hall_sensor_read();
        if((hall_low && hall > HALL_THRESHOLD_HIGH) || (hall_high && hall < HALL_THRESHOLD_LOW)){
            return true;
        }
    }
    return false;
}

void app_main()
{

    //xTaskCreate(test_task, "", 4096, NULL, 10, NULL);
    if(check_factory_reset()){
        ESP_LOGW(TAG, "ERASING CONFIG");
        reset_config();
    }
    if(initialize_system_config()!=ESP_OK){
        //        factory_reset();
        esp_restart();
    }
    esp_log_level_set("phy_init", ESP_LOG_INFO);
    ESP_LOGI(TAG, "Device name: %s", systemConfig->device_name);
    ESP_LOGI(TAG, "Firmware version: %s", systemStatus->firmware_version);
    wifi_init();
    sync_init();
    ota_init();
    ESP_ERROR_CHECK(ble_init());
    vTaskDelay(pdMS_TO_TICKS(10000));
    esp_ota_mark_app_valid_cancel_rollback();
}
